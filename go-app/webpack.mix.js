const mix = require('laravel-mix')
const webpack = require('webpack')
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
// mix.js('node_modules/datatables.net/js/jquery.dataTables.js', 'public/js')
// mix.js('node_modules/datatables.net-bs4/js/dataTables.bootstrap4.min.js', 'public/js')
// mix.copy('node_modules/datatables.net-bs4/css/dataTables.bootstrap4.min.css', 'public/css')

mix.js('resources/js/app.js', 'public/js')
mix.sass('resources/sass/app.scss', 'public/css')

// mix.copy('resources/js/info.js', 'public/js')

mix.webpackConfig({
  plugins: [
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery'
    //            Tablesaw: 'tablesaw'
    })
  ]
})

mix.autoload({
  jquery: ['$', 'window.jQuery', 'jQuery']
})
