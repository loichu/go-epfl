Feature:
    The goal is to have a page where one can reveal the url behind an alias

    @database
    Scenario: Reveal page exists
        Given "ggl" alias exists for "https://google.com"
        When I am on "/reveal/ggl"
        Then I should see "https://google.com"

    Scenario: 404 of nonexistent alias
        Given I am on "/reveal/ggl"
        Then the response status code should be 404
