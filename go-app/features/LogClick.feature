Feature:
    Clicks are correctly logged for stats.

    @database
    Scenario: No stats displayed if there is no clicks
        Given "ggl" alias exists for "https://google.com"
        When I am on "/info/ggl"
        Then I should see "Sorry but you have no hits yet."

    @database
    Scenario: Access to alias is logged
        Given "ggl" alias exists for "https://google.com"
        And I browse alias "ggl"
        When I am on "/info/ggl"
        Then I should see "Total hits: 1" in the "div#total_hits" element
