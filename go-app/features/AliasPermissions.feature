Feature:
    Aliases should be owned by owners who are the only people allowed to edit/delete them.

    @database
    Scenario: Owned aliases appear on profile page
        Given I am a sample user
        And "test" alias exists for "https://test.com"
        And I am on the homepage
        When I fill in "urlInput" with "https://google.com"
        And I fill in "aliasInput" with "ggl"
        And I press "Shorten URL"
        And I am on "profile"
        Then I should see "ggl" in the "div#aliasesList" element
        And I should not see "test" in the "div#aliasesList" element

    @database
    Scenario: Aliases are editable
        Given I am on my profile page
        And I follow "edit"
        Then the response status code should be 200

    @database
    Scenario: Edit form is pre-filled
        Given I own "ggl" alias redirecting to "https://google.com"
        And I am on "/edit/alias/ggl"
        Then the "alias" field should contain "ggl"
        And the "url" field should contain "https://google.com"

    @database
    Scenario: Edit form is protected
        Given I own "ggl" alias redirecting to "https://google.com"
        And "test" alias exists for "test.com"
        And I am on "/edit/alias/test"
        Then the response status code should be 403

    @database
    Scenario: 404 is returned if alias does not exist
        Given I own "ggl" alias redirecting to "https://google.com"
        And I am on "/edit/alias/test"
        Then the response status code should be 404

    @database
    Scenario: Duplicated alias error is handled
        Given I own "ggl" alias redirecting to "https://google.com"
        And "double" alias exists for "double.com"
        When I am on "/edit/alias/ggl"
        And I fill in "alias" with "double"
        And I press "Update"
        Then I should be on "/edit/alias/ggl"
        And I should see "The alias has already been taken."

    @database
    Scenario: The aliases are editable
        Given I own "ggl" alias redirecting to "https://google.com"
        And I am on my profile page
        When I follow "edit"
        And I fill in "alias" with "test"
        And I press "Update"
        Then I should be on "profile"
        And I should see "test" in the "div#aliasesList" element
        And I should see "The alias has been updated"

    @database
    Scenario: The urls are editable
        Given I own "ggl" alias redirecting to "https://google.com"
        And I am on my profile page
        When I follow "edit"
        And I fill in "url" with "test"
        And I press "Update"
        Then I should be on "profile"
        And I should see "test" in the "div#aliasesList" element
        And I should see "The alias has been updated"
