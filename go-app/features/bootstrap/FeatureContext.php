<?php

use App\BlacklistItem;
use Behat\Behat\Hook\Scope\AfterStepScope;
use Behat\Behat\Tester\Exception\PendingException;
use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
#This will be needed if you require "behat/mink-selenium2-driver"
#use Behat\Mink\Driver\Selenium2Driver;
use Behat\Mink\Exception\ElementNotFoundException;
use Behat\MinkExtension\Context\MinkContext;
use Behat\Behat\Hook\Scope\AfterScenarioScope;
use Behat\Behat\Context\SnippetAcceptingContext;
use \App\User;
use \App\Alias;
use \App\Url;
use \App\Log;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Behat\Mink\Exception\UnsupportedDriverActionException;
use Behat\Mink\Driver\GoutteDriver;

/**
 * Defines application features from the specific context.
 */
class FeatureContext extends MinkContext implements Context, SnippetAcceptingContext
{
    private $pub_aliases_count = 0;
    private $command_response = 0;
    private $current_user;
    private $json_response;

    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct()
    {
    }

    public function canIntercept()
    {
        $driver = $this->getSession()->getDriver();
        if (!$driver instanceof GoutteDriver) {
            throw new UnsupportedDriverActionException(
                'You need to tag the scenario with '.
                '"@mink:goutte" or "@mink:symfony2". '.
                'Intercepting the redirections is not '.
                'supported by %s', $driver
            );
        }
    }

    /**
     * @Given /"([^"]*)" is in the blacklist/
     *
     */
    public function wordIsInBlacklist($keyword)
    {
        $word = new BlacklistItem;
        $word->keyword = $keyword;
        $word->save();
    }

    /**
     * @When /I send "([^"]*)" to "([^"]*)"/
     *
     */
    public function iSendParamsToApiEndpoint($params, $endpoint)
    {
        $params = explode(',', $params);
        foreach ($params as $index =>$param)
        {
            $key_value = explode('=', $param);
            $params[$key_value[0]] = $key_value[1];
            unset($params[$index]);
        }

        $method_url = explode(' ', $endpoint);
        $method = $method_url[0];
        $url = $method_url[1];
        $params['token'] = $this->current_user->api_token;
        $query_string = http_build_query($params);

        $ch = curl_init(config('app.url').$url);
        curl_setopt($ch, constant("CURLOPT_$method"), 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $query_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);
        $this->json_response = curl_exec($ch);
        echo var_export($this->json_response, true);
        echo config('app.url');
        curl_close($ch);
    }

    /**
     * @When /I am not logged in/
     *
     */
    public function iAmNotLoggedIn()
    {
        Auth::logout();
    }

    /**
     * @Then /the json response should contain "([^"]*)"="([^"]*)"$/
     *
     */
    public function jsonResponseShouldContain($key, $value)
    {
        $response_array = json_decode($this->json_response);
        if (!$response_array[$key] == $value)
            throw new Exception('Expected value for '.$key.': '.$value.', got: '.$response_array[$key]);
    }

    /**
     * @Then /"([^"]*)" alias should have obsolescence_date set to "([^"]*)"/
     *
     */
    public function aliasShouldHaveObsolescenceDateSetTo($alias, $date)
    {
        $alias = Alias::where('alias', $alias)->first();
        if (!$alias->obsolescence_date == date('Y-m-d', strtotime($date)))
            throw new Exception('Expected obsolescence date: '.date('Y-m-d', strtotime($date)).', got: '.$alias->obsolescence_date);
    }

    /**
     * @Then /"([^"]*)" alias should not have obsolescence_date set/
     *
     */
    public function aliasShouldNotHaveObsolescenceDateSet($alias)
    {
        $alias = Alias::where('alias', $alias)->first();
        if (isset($alias->obsolescence_date))
            throw new Exception('Expected obsolescence date was set but should not');
    }

    /**
     * @Given /"([^"]*)" alias is obsolete/
     *
     */
    public function aliasIsObsolete($alias)
    {
        $alias = Alias::where('alias', $alias)->first();
        $alias->obsolescence_date = date('Y-m-d', strtotime('-1 month'));
        $alias->save();
    }

    /**
     * @When /my API token is "([^"]*)"/
     *
     */
    public function myApiTokenIs($token)
    {
        $this->current_user->api_token = $token;
    }

    /**
     * @Given /I launch "([^"]*)" command with "([^"]*)" as "([^"]*)" parameter/
     *
     */
    public function launchCommandWithParameter($command, $p_value, $p_name)
    {
        Artisan::call($command, [$p_name => $p_value]);
        $this->command_response = (int) Artisan::output();
    }

    /**
     * @Given /I launch "([^"]*)" command with the following parameters:/
     *
     */
    public function launchCommandWithParameters($command, TableNode $parameters)
    {
        Artisan::call($command, $parameters->getHash()[0]);
        $this->command_response = (int) Artisan::output();
    }

    /**
     * @When /I print the command's result/
     *
     */
    public function printCommandResult()
    {
        echo $this->command_response;
    }

    /**
     * @Then /The command's result should be "([^"]*)"/
     *
     */
    public function commandResultShouldBe($expected)
    {
        if ($expected != $this->command_response) {
            throw new \Exception(
                "Expected response: $expected\n" .
                "Actual response: $this->command_response"
            );
        }
    }

    /**
     * @Given /I am a sample user/
     *
     */
    public function iAmASampleUser()
    {
        $user = User::firstOrCreate([
            'email'     => 'user@sample.com',
            'sciper'    => '123456',
            'token'     => 'Bearer sample',
            'firstname' => 'Sample',
            'lastname'  => 'User',
            'username'  => 'suser',
        ]);
        Auth::login($user);
        $this->current_user = $user;
    }

    /**
     * @Then /^I should not see an "([^"]*)" field$/
     */
    public function iShouldNotSeeAnField($field_name)
    {
        $page = $this->getSession()->getPage();
        $field = $page->findField($field_name);
        // Try to resolve by ID.
        $field = $field ? $field : $page->findById($field_name);

        if ($field !== NULL) {
            throw new \Exception(sprintf('A field "%s" appears on this page, but it should not.', $field_name));
        }
    }

    /**
     * @Then /^I should see an "([^"]*)" field$/
     */
    public function iShouldSeeAnField($field_name)
    {
        $page = $this->getSession()->getPage();
        $field = $page->findField($field_name);
        // Try to resolve by ID.
        $field = $field ? $field : $page->findById($field_name);

        if ($field === NULL) {
            throw new ElementNotFoundException($this->getSession()
                ->getDriver(), 'form field', 'id|name|label|value', $field_name);
        }

        return $field;
    }

    /**
     * @Then /^the URL "([^"]*)" should have a "([^"]*)"$/
     */
    public function urlShouldHave($url, $attribute)
    {
        $url = Url::where('url', $url)->first();

        if ($url->$attribute === NULL) {
            throw new Exception("Attribute $attribute is null");
        }
    }

    /**
     * @Then /^the URL "([^"]*)" should not exist$/
     */
    public function urlShouldNotExist($url)
    {
        $url_exists = Url::where('url', $url)->first();

        if ($url_exists) {
            throw new Exception("The url $url exists but shouldn't");
        }
    }

    /**
     * @Then /^the alias "([^"]*)" should be associated with "([^"]*)"$/
     */
    public function urlShouldBeAssociatedWith($alias, $url)
    {
        $alias = Alias::where('alias', $alias)->first();

        if ($alias->url->url !== $url) {
            throw new Exception("Alias not associated with $url");
        }
    }

    /**
     * @Given /^"([^"]*)" alias exists for "([^"]*)"$/
     */
    public function aliasExists($aliasInput, $urlInput)
    {
        $url = Url::firstOrCreate(['url' => $urlInput]);
        $alias = new Alias;
        $alias->alias = $aliasInput;
        $alias->hidden = false;
        $alias->url()->associate($url);
        $alias->save();
    }

    /**
     * @Given /^I am on my profile page$/
     */
    public function iAmOnMyProfilePage()
    {
        $this->iAmASampleUser();
        $this->iAmOnHomepage();
        $this->iRegisterAliasFor('ggl', 'https://google.com');
        $this->visit('profile');
    }

    /**
     * @When /^I register "([^"]*)" alias for "([^"]*)"$/
     */
    public function iRegisterAliasFor($aliasInput, $urlInput)
    {
        $this->fillField('alias', $aliasInput);
        $this->fillField('url', $urlInput);
        $this->pressButton('Shorten URL');
    }

    /**
     * @Given /^I own "([^"]*)" alias redirecting to "([^"]*)"$/
     */
    public function iOwnAlias($aliasInput, $urlInput)
    {
        $this->iAmASampleUser();
        $this->iAmOnHomepage();
        $this->fillField('alias', $aliasInput);
        $this->fillField('url', $urlInput);
        $this->pressButton('Shorten URL');
    }

    /**
     * @AfterScenario @database
     */
    public function cleanDB(AfterScenarioScope $scope)
    {
        $aliases = array('ggl', 'test', 'double', 'browse', 'gogl', 'isis');
        foreach ($aliases as $alias) {
            $alias = Alias::where('alias', $alias)->first();
            if (isset($alias)) {
                $alias->owners()->detach();
                $alias->delete();
            }
        }

        $urls = array('https://google.com', 'google.com');
        foreach ($urls as $url) {
            $url = Url::where('url', $url);
            $url->forceDelete();
        }

        User::where('email', 'user@sample.com')->delete();

        BlacklistItem::where('keyword', 'isis')->delete();
    }

    /**
     * @Given /^I count public aliases$/
     */
    public function countPublicAliases()
    {
        $this->pub_aliases_count = Alias::public()->count();
    }

    /**
     * @When /^I report the "([^"]*)" alias$/
     */
    public function reportAlias($alias)
    {
        $alias = Alias::where('alias', $alias)->first();

        $flag = new \App\Flag();
        $flag->flag_type = \App\FlagType::REPORT;
        $flag->details = array(
            'sciper' => 123456,
            'email'  => 'email@example.com',
            'message' => 'Inappropriate'
        );

        $alias->flags()->save($flag);
    }

    /**
     * @Then /^I should have (\d+) public alias(es)? less$/
     */
    public function compareAliasCountLess($nb_aliases_less)
    {
        $actual = Alias::public()->count();
        $expected = $this->pub_aliases_count - $nb_aliases_less;
        if ($expected != $actual)
        {
            throw new Exception(
                "Actual number of public aliases is: $actual, expected: $expected"
            );
        }
    }

    /**
     * @When /^I follow the redirection$/
     * @Then /^I should be redirected$/
     */
    public function iFollowTheRedirection()
    {
        $this->canIntercept();
        $client = $this->getSession()->getDriver()->getClient();
        $client->followRedirects(true);
        $client->followRedirect();
    }

    /**
     * @Given /^I am on alias "([^"]*)" before redirection$/
     * @Given /^I am on path "([^"]*)" before redirection$/
     */
    public function theRedirectionsAreIntercepted($alias)
    {
        $this->canIntercept();
        $this->getSession()->getDriver()->getClient()->followRedirects(false);
        $this->visitPath('/'.$alias);
    }

    /**
     * @Given /^I browse alias "([^"]*)"$/
     */
    public function iBrowseAlias($alias)
    {
        $this->visitPath('/'.$alias);
    }

    /**
     * This works for the Goutte driver and I assume other HTML-only ones.
     *
     * @Then /^show me the HTML page$/
     */
    public function show_me_the_html_page_in_the_browser() {
        $html_data = $this->getSession()->getDriver()->getContent();
        dd($html_data);
        $file_and_path = '/tmp/behat_page.html';
        file_put_contents($file_and_path, $html_data);
        var_export($html_data);
    }
}
