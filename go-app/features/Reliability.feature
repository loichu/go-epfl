Feature:
    The reliability test performs an HTTP request on the URLs and returns the resulting status code

    Scenario Outline: The commands returns the correct status code
        Given I launch "test:reliability" command with "<url>" as "url" parameter
        When I print the command's result
        Then The command's result should be "<expected status code>"
    Examples:
        | url                     | expected status code |
        | http://httpstat.us/200  | 200                  |
        | http://httpstat.us/300  | 300                  |
        | http://httpstat.us/400  | 400                  |
        | https://httpstat.us/400 | 400                  |
        | http://httpstat.us/500  | 500                  |
        | http://httpstat.us/500  | 500                  |
        | httpstat.us/200         | 200                  |
        | notanurl                | 0                    |
        | http://notanurl.com     | 0                    |

    @database
    Scenario: The command persists correctly the status
        Given "ggl" alias exists for "https://google.com"
        When I launch "test:reliability" command with the following parameters:
            | url                | --persist    |
            | https://google.com | true         |
        Then the URL "https://google.com" should have a "status_code"

    @database
    Scenario: The replaces correctly the URL
        Given "gogl" alias exists for "google.com"
        And "ggl" alias exists for "https://google.com"
        When I launch "test:reliability" command with the following parameters:
            | url                | --persist    |
            | google.com         | true         |
        Then the alias "gogl" should be associated with "https://google.com"
