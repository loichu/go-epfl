Feature:
    The aliases are flaggable and flagged alias behave differently.

    Scenario: Logged in users should be able to report aliases
        Given I am a sample user
        When I am on "browse"
        And I follow "report"
        Then I should be on "reportconfirm"
        And I should see "Thank you for helping us to keep this platform appropriate !"

    @database
    Scenario: Alias disappears from public scope
        Given "ggl" alias exists for "https://google.com"
        And I count public aliases
        When I report the "ggl" alias
        Then I should have 1 public alias less

    @database
    Scenario: Unauthenticated users gets redirected to login on report/{alias}
        Given "ggl" alias exists for "https://google.com"
        And I am not logged in
        When I am on "/report/ggl"
        Then I should be on "https://tequila.epfl.ch/OAUTH2IdP/auth"

    @database
    Scenario: URL is revealed before redirect from flagged alias and a warning message explains why
        Given "ggl" alias exists for "https://google.com"
        And I report the "ggl" alias
        When I am on "/ggl"
        Then the response status code should be 200
        And I should be on "/ggl"
        And I should see "https://google.com"
        And I should see "This alias has been reported!"

        Scenario: Visitors cannot report aliases
            Given I am on "browse"
            Then I should not see "report"
