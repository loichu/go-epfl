Feature:
    Users can define a TTL (Time To Live) for their aliases. The alias should not be accessible after
    that time.

    @database
    Scenario: The time to live is registered in the database
        Given I am a sample user
        And I am on the homepage
        When I fill in "urlInput" with "https://google.com"
        And I fill in "aliasInput" with "ggl"
        And I select "6" from "ttl"
        And I press "Shorten URL"
        Then "ggl" alias should have obsolescence_date set to "+6 months"

    @database
    Scenario: The time to live is registered in the database
        Given I am a sample user
        And I am on the homepage
        When I fill in "urlInput" with "https://google.com"
        And I fill in "aliasInput" with "ggl"
        And I select "0" from "ttl"
        And I press "Shorten URL"
        Then "ggl" alias should not have obsolescence_date set

    @database
    Scenario: An obsolete alias should not be accessible
        Given "ggl" alias exists for "https://google.com"
        And "ggl" alias is obsolete
        When I am on "/ggl"
        Then the response status code should be 404
