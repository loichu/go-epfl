<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertAdminUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Insert Nicolas as admin
        DB::table('users')->updateOrInsert(
            ['sciper' => '169419'],
            [
                'firstname' => 'Nicolas',
                'lastname' => 'Borboën',
                'email' => 'nicolas.borboen@epfl.ch',
                'sciper' => '169419',
                'username' => 'nborboen',
                'token' => '',
                'is_admin' => true
            ]
        );
        // Insert Loïc as admin
        DB::table('users')->updateOrInsert(
            ['sciper' => '262563'],
            [
                'firstname' => 'Loïc',
                'lastname' => 'Humbert',
                'email' => 'loic.humbert@epfl.ch',
                'sciper' => '262563',
                'username' => 'ldhumber',
                'token' => '',
                'is_admin' => true
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Be sure to remove foreign key constraint first
        DB::table('permissions')->where('user_id', 1)->delete();
        DB::table('permissions')->where('user_id', 2)->delete();
        // Remove users
        DB::table('users')->where('sciper', 169419)->delete();
        DB::table('users')->where('sciper', 262563)->delete();
    }
}
