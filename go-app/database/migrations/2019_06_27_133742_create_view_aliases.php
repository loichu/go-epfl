<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateViewAliases extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("CREATE OR replace FUNCTION truncateURL(url text, OUT result text) AS
$$
BEGIN
    IF char_length(url) > 99 THEN
    	result := concat_ws('[...]', substring(url, 0, 50), substring(url from char_length(url)-45 for 45));
    else
    	result := url;
    END IF;
    RETURN;
END;
$$
LANGUAGE plpgsql;");

        // Sorry if your linter's fucked up with this one...
        DB::statement("CREATE OR REPLACE VIEW public.v_aliases
                        AS SELECT a.id, a.alias, url.url, truncateURL(url.url) as trucatedurl, a.created_at, a.updated_at, a.hidden, url.status_code, u.sciper, u.username,
                            (SELECT COUNT(l.id) FROM public.logs l WHERE l.alias_id = a.id) AS clicks,
                           	(SELECT COUNT(f.id) FROM public.flags f WHERE ((f.object_id = a.id) AND (f.object_type = 'App\\Alias'))) AS flags,
                           	a.obsolete
                        FROM public.aliases a
                        LEFT JOIN public.urls url ON (a.url_id = url.id)
                        LEFT JOIN public.permissions p ON (p.alias_id = a.id)
                        LEFT JOIN public.users u ON (p.user_id = u.id)
                        ORDER BY a.id DESC;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW v_aliases");
        DB::statement("DROP FUNCTION IF EXISTS truncateURL");
    }
}
