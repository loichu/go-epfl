<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // One random alias linked to a random URL
        factory(App\Alias::class)->create([
            'url_id' => factory(App\Url::class)->create()->id
        ]);

        // One simple user
        factory(\App\User::class)->create();

        // One admin user
        factory(\App\User::class)->state('admin')->create();
    }
}
