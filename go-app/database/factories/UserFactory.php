<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    return [
        'sciper' => $faker->unique()->randomNumber(6),
        'firstname' => ($fname = $faker->firstName),
        'lastname' => ($lname = $faker->unique()->lastName),
        'username' => substr(lcfirst($fname), 0, 1).strtolower($lname),
        'email' => strtolower($fname).'.'.strtolower($lname).'@epfl.ch',
        'remember_token' => str_random(10),
        'token' => 'Bearer fake'
    ];
});

$factory->state(App\User::class, 'admin', [ 'is_admin' => true ]);
