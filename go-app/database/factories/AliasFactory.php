<?php

use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Alias::class, function (Faker $faker) {
    return [
        'alias'    => $faker->unique()->lexify('???????'),
        'hidden'   => false,
        'obsolete' => false,
    ];
});
