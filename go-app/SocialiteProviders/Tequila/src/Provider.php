<?php

namespace SocialiteProviders\Tequila;

use Laravel\Socialite\Two\ProviderInterface;
use SocialiteProviders\Manager\OAuth2\AbstractProvider;
use SocialiteProviders\Manager\OAuth2\User;

class Provider extends AbstractProvider implements ProviderInterface
{
    /**
     * Unique Provider Identifier.
     */
    const IDENTIFIER = 'TEQUILA';

    /**
     * {@inheritdoc}
     */
    protected $scopes = ['Tequila.profile'];

    private const BASE_URL = 'https://tequila.epfl.ch';

    /* User additional info fields
    *
    * By default returns: id, first_name, last_name
    *
    * @var array
    */
    // protected $infoFields = ['name', 'email'];

    /**
     * {@inheritdoc}
     */
    protected function getAuthUrl($state)
    {
        return $this->buildAuthUrlFromBase(self::BASE_URL.'/OAUTH2IdP/auth', $state);
    }

    /**
     * {@inheritdoc}
     */
    protected function getTokenUrl()
    {
        return self::BASE_URL.'/OAUTH2IdP/token';
    }

    /**
     * {@inheritdoc}
     */
    protected function getUserByToken($token)
    {
        //error_log(var_export(debug_backtrace(), true));
        $response = $this->getHttpClient()->get(self::BASE_URL.'/OAUTH2IdP/userinfo', [
            'headers' => [
                'Authorization' => $token,
            ],
        ]);

        return json_decode($response->getBody(), true);
    }

    /**
     * {@inheritdoc}
     */
    protected function mapUserToObject(array $user)
    {
        return (new User())->setRaw($user)->map([
            'name'      => $user['Name'],
            'firstname' => $user['Firstname'],
            'email'     => $user['Email'],
            'sciper'    => $user['Sciper'],
            'username'  => $user['Username']
        ]);
    }

    /**
     * {@inheritdoc}
     */
    protected function getTokenFields($code)
    {
        return array_merge(parent::getTokenFields($code), [
            'grant_type' => 'authorization_code',
            "scope" => implode(" ", $this->scopes)
        ]);
    }
}
