<?php
/**
 * Created by IntelliJ IDEA.
 * User: loichu
 * Date: 23.05.19
 * Time: 10:28
 */

namespace Tests\Feature;

use App\Alias;
use App\Url;
use App\User;
use Illuminate\Support\Facades\Log;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Routing\Router;
use Laracasts\Behat\Context\DatabaseTransactions;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Response;

class AdminDashboardTest extends TestCase
{
    public function getAdminUris()
    {
        $admin_routes = array();
        foreach (app()->routes->getRoutes() as $route)
            if (trim($route->getPrefix(), '/') == 'admin'
              && in_array('GET', $route->methods)
              && !preg_match('@^admin/jobs@', $route->uri))
                $admin_routes []= $route->uri;
        return $admin_routes;
    }

    public function getUri($uri)
    {
        $alias = Alias::find(1);
        $uri = preg_replace('/{alias}/', $alias->alias, $uri);
        return $this->get($uri);
    }

    /**
     * Admin pages are forbidden for guests
     *
     */
    public function testGuestsForbidden()
    {
        foreach ($this->getAdminUris() as $uri)
        {
            $response = $this->getUri($uri);
            $response->assertStatus(Response::HTTP_FORBIDDEN);
        }
    }

    /**
     * Admin pages are forbidden for users
     *
     */
    public function testUsersForbidden()
    {
        foreach ($this->getAdminUris() as $uri)
        {
            $user = User::where('is_admin', false)->first();
            $response = $this->actingAs($user)
                             ->getUri($uri);
            $response->assertStatus(Response::HTTP_FORBIDDEN);
        }
    }

    /**
     * Admin pages are allowed for admins
     *
     */
    public function testAdminsAllowed()
    {
        $user = User::where('is_admin', true)->first();
        foreach ($this->getAdminUris() as $uri)
        {
            $response = $this->actingAs($user)
                             ->getUri($uri);
            $response->assertStatus(Response::HTTP_OK);
        }
    }
}
