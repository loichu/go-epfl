<?php
/**
 * Created by IntelliJ IDEA.
 * User: loichu
 * Date: 24.04.19
 * Time: 13:54
 */

namespace App\Traits;


use Illuminate\Pagination\LengthAwarePaginator;

trait Pagination
{
    private function pagination(LengthAwarePaginator $results)
    {
        $current_page = $results->currentPage();
        $first_page_displayed =
            (
            ($results->onFirstPage()) ? 'current' :
                ((1 < $current_page - 2) ? $current_page - 2 : 1)
            );
        $last_page_displayed =
            (
            (!$results->hasMorePages()) ? 'current' :
                (($results->lastPage() > $current_page + 2)
                    ? $current_page + 2 : $results->lastPage())
            );

        $displayed_pages = array();
        if ($first_page_displayed === 'current' || $last_page_displayed === 'current')
        {
            if ($first_page_displayed === 'current') {
                $displayed_pages['before'] = array();
                $displayed_pages['after'] =
                    $last_page_displayed === 'current'
                        ? array()
                        : $results->getUrlRange($current_page+1, $last_page_displayed);
            } elseif ($last_page_displayed === 'current')
                $displayed_pages = array (
                    'before' => $results->getUrlRange($first_page_displayed, $current_page-1),
                    'after'  => array()
                );
        } else {
            $displayed_pages = array (
                'before' => $results->getUrlRange($first_page_displayed, $current_page-1),
                'after'  => $results->getUrlRange($current_page+1, $last_page_displayed)
            );
        }

        return $displayed_pages;
    }
}
