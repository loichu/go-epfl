<?php
/**
 * Created by IntelliJ IDEA.
 * User: loichu
 * Date: 18.04.19
 * Time: 09:24
 */

namespace App\Console;


use App\Url;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Log;

class ReliabilityTest
{
    public function __invoke()
    {
        $urls = Url::all();

        foreach ($urls as $url)
        {
            Artisan::call('test:reliability', ['url' => $url->url, '--persist' => true]);
            Log::info("Reliability test: ", [$url->url, Artisan::output()]);
        }
    }
}
