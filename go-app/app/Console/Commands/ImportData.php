<?php

namespace App\Console\Commands;

use App\Url;
use Dotenv\Exception\ValidationException;
use Illuminate\Console\Command;
//use Illuminate\Contracts\Validation\Validator;
use Illuminate\Support\Facades\DB;
use App\Log;
use App\Alias;
use App\Rules\Alias as AliasRule;
use App\Rules\URL as UrlRule;
use Symfony\Component\Console\Helper\ProgressBar;

class ImportData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import data from csv files placed into database/csv to the database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $urls = $this->csv_to_assoc('database/csv/urls.csv');
        $logs = $this->csv_to_assoc('database/csv/logs.csv');

        $nb_urls = count($urls);
        $nb_logs = count($logs);

        $this->info('There are '.$nb_urls.' urls and '.$nb_logs.' logs.');
        if ($this->confirm('Do you wish to continue?')) {
            $bar = $this->output->createProgressBar($nb_urls + $nb_logs);

            $bar->start();

            $this->import_data($logs, $urls, $bar);

            $bar->finish();

            $this->info("\n Success ! \n");
        }
    }

    private function import_data($logs, $urls, ProgressBar $bar)
    {
        try {
            DB::beginTransaction();
            $this->importURLs($urls, $bar);
            DB::commit();
            DB::beginTransaction();
            $this->importLogs($logs, $bar);
            DB::commit();
        } catch (\Exception $e)
        {
            DB::rollback();
            $this->exit($bar, $e);
        }
    }

    private function csv_to_assoc($file)
    {
        $csv = array_map('str_getcsv', file($file));
        array_walk($csv, function(&$a) use ($csv) {
            $a = array_combine($csv[0], $a);
        });
        array_shift($csv); # remove column header
        return $csv;
    }

    private function importURLs($urls, ProgressBar $bar)
    {
        foreach ($urls as $url) {
            $related_url = Url::firstOrCreate(['url' => $url['url']]);

            $alias = new Alias;
            $alias->alias = $url['alias'] ?: $url['code'];
            $alias->created_at = $url['date_added'];
            $alias->url()->associate($related_url);
            $alias->hidden = $url['private'];
            //$validator = $alias->validate();

            /*if ($validator->fails()) {
                throw new ValidationException();
            }*/

            $alias->saveOrFail();

            $bar->advance();
        }
    }

    private function importLogs($logs, ProgressBar $bar)
    {
        foreach ($logs as $index => $log) {
            $related_alias = Alias::where('alias', $log['shorturl'])->first();

            $log_record = new Log;
            $log_record->click_time = $log['click_time'];
            $log_record->referrer = $log['referrer'];
            $log_record->user_agent = $log['user_agent'];
            $log_record->ip_address = $log['ip_address'];
            $log_record->iso_code = $log['country_code'];
            $log_record->alias_id = $related_alias->id;
            $log_record->alias = $log['shorturl'];
            $log_record->url = $related_alias->url->url;
            $log_record->saveOrFail();

            $bar->advance();
        }
    }

    private function exit(ProgressBar $bar, \Exception $exception)
    {
        $bar->clear();
        $this->error($exception->getMessage().'\n'.$exception->getTraceAsString());
        exit(255);
    }
}
