<?php

namespace App\Console\Commands;

use App\Url;
use Illuminate\Console\Command;
use Illuminate\Validation\ValidationException;

class ReliabilityCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test:reliability {url} {--persist}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Test the HTTP status code of an URL';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $url      = $this->argument('url');
        $persist  = $this->option('persist');
        $base_url = $url;

        if (! preg_match('%^https?://%', $url)) {
            $url = 'https://'.$url;
            if (! $status_code = $this->get_http_response_code($url))
                $url = 'http://'.$base_url;
        }

        if (!isset($status_code))
            $status_code = $this->get_http_response_code($url);

        if ($persist) {
            $url_obj = Url::where('url', $base_url)->first();

            if($url !== $base_url && $status_code !== 0)
                $this->updateURL($url_obj, $url);

            $this->updateStatus($url_obj, $status_code);
        }

        $this->line($status_code);
    }

    public function get_http_response_code($url)
    {
        try {
            $headers = get_headers($url);
        } catch (\Exception $e) {
            $headers = false;
        }

        if ($headers)
            return substr($headers[0], 9, 3);
        else
            return 0;
    }

    public function updateURL(Url $url, $new_url)
    {
        $same_url = Url::where('url', $new_url)->first();
        if (empty($same_url)) {
            $url->timestamps = false;
            $url->url = $new_url;
            $url->saveOrFail();
        } else {
            foreach ($url->aliases as $alias) {
                $alias->url()->dissociate();
                $alias->url()->associate($same_url);
                $alias->saveOrFail();
            }
            //echo "Force delete url $url->url";
            $url->forceDelete();
        }
    }

    public function updateStatus(Url $url, $status)
    {
        $url->timestamps  = false;
        $url->status_code = $status;
        $url->tested_at   = date('Y-m-d H:i');
        $url->saveOrFail();
    }
}
