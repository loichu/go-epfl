<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use PDO;
use PDOException;

/**
 * Class DatabaseCreateCommand
 * Found here: https://anthonysterling.com/posts/using-an-artisan-command-to-create-a-database-in-laravel-or-lumen.html
 *
 * This command is not generic. MySQL implementation can be found by following the above link.
 * Creates a PostgreSQL database.
 *
 * @package App\Console\Commands
 */
class DatabaseCreateCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'db:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command creates a new PostgreSQL database';

    /**
     * The console command signature.
     *
     * @var string
     */
    protected $signature = 'db:create';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $this->create_database();
        $this->create_database('test');
    }

    private function create_database($suffix = '')
    {
        $default_connection = env('DB_CONNECTION');
        $connection_details = config('database.connections.'.$default_connection);
        $database = empty($suffix)
            ? $connection_details['database']
            : $connection_details['database'].'_'.$suffix;

        if (! $database) {
            $this->info('Skipping creation of database as env(DB_DATABASE) is empty');
            return;
        }

        try {
            $pdo = $this->getPDOConnection(env('DB_CONNECTION'), env('DB_HOST'), env('DB_PORT'), env('DB_USERNAME'), env('DB_PASSWORD'));

            $pdo->exec($this->buildRequest($database, $connection_details));

            $this->info(sprintf('Successfully created %s database', $database));
        } catch (\Exception $exception) {
            $this->error(sprintf('Failed to create %s database, %s', $database, $exception->getMessage()));
        }
    }

    private function buildRequest($database, array $connection_details)
    {
        $base_request = sprintf(
            'CREATE DATABASE %s',
            $database
        );
        $base_request .= !isset($connection_details['charset']) ?'': sprintf(
            ' ENCODING %s',
            $connection_details['charset']
        );
        $base_request .= !isset($connection_details['collation']) ?'': sprintf(
            ' LC_COLLATE %s',
            $connection_details['collation']
        );

        $final_request = $base_request.';';
        return $final_request;
    }

    /**
     * @param  connection
     * @param  string $host
     * @param  integer $port
     * @param  string $username
     * @param  string $password
     * @return PDO
     */
    private function getPDOConnection($connection, $host, $port, $username, $password)
    {
        return new PDO(sprintf('%s:host=%s;port=%d;', $connection, $host, $port), $username, $password);
    }
}
