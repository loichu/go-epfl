<?php namespace App\YOURLS;
/**
 * Created by IntelliJ IDEA.
 * User: loichu
 * Date: 13.03.19
 * Time: 13:54
 */

use Illuminate\Support\Facades\DB;

trait GraphsTrait
{
    use YourlsTrait;

    public function getSortedReferrers($referrers)
    {
        $sorted_referrers = $this->sortedReferrers($referrers);
        $detailed_referrers = array();


        foreach ($sorted_referrers as $host => $count)
        {
            $detailed_referrers[$host]['count']   = $count;
            $detailed_referrers[$host]['favicon'] = $this->yourls_get_favicon_url( $host );
            $detailed_referrers[$host]['sites']   = array();

            foreach ($referrers[$host] as $site => $count)
            {
                $detailed_referrers[$host]['sites'][$site] = $count;
            }
        }

        return $detailed_referrers;
    }

    public function getCountriesDetails($countries)
    {
        $restcountriesurl = "https://restcountries.eu/rest/v2/alpha/";

        $detailed_countries = array();
        foreach ($countries as $iso_code => $clicks)
        {
            if (empty($iso_code))
                continue;
            try {
                $json_country  = file_get_contents($restcountriesurl.$iso_code);
            } catch (\Exception $e){continue;}

            $country_array = json_decode($json_country);
            $detailed_countries[$iso_code]['flag']         = $country_array->flag;
            $detailed_countries[$iso_code]['country_name'] = $country_array->name;
            $detailed_countries[$iso_code]['hits']         = $clicks;
        }

        return $detailed_countries;
    }

    /**
     * Gets all referrers, most frequent first.
     *
     * @param $alias_id
     * @return array
     */
    private function getReferrers($alias_id)
    {
        $referrers = array();
        $referrer_rows = DB::table('logs')
            ->select('referrer', DB::Raw('count (*) as count'))
            ->where(['alias_id' => $alias_id])
            ->groupBy('referrer')
            ->get();


        $referrers['notdirect'] = 0;
        $referrers['direct'] = 0;
        // Loop through all results and build list of referrers, countries and hits per day
        foreach ($referrer_rows as $row)
        {
            if ( $row->referrer == 'direct' ) {
                $referrers['direct'] = $row->count;
                continue;
            }

            $referrers['notdirect'] += $row->count;

            $host = $this->yourls_get_domain( $row->referrer );
            if( !array_key_exists( $host, $referrers ) )
                $referrers[$host] = array( );
            if( !array_key_exists( $row->referrer, $referrers[$host] ) ) {
                $referrers[$host][$row->referrer] = $row->count;
            } else {
                $referrers[$host][$row->referrer] += $row->count;
            }
        }

        arsort( $referrers );
        return $referrers;
    }

    /**
     * Gets an array of most frequent domains.
     * Accepts either already fetched referrers or related alias_id.
     *
     * @param array|null $referrers
     * @param int $alias_id
     * @return array
     */
    private function sortedReferrers($referrers = array(), $alias_id = 0)
    {
        if ($alias_id !== 0)
            $referrers = $this->getReferrers($alias_id);

        unset($referrers['direct']);
        unset($referrers['notdirect']);

        $referrer_sort = array();
        $number_of_sites = count( array_keys( $referrers ) );
        foreach( $referrers as $site => $urls ) {
            if( count($urls) > 1 || $number_of_sites == 1 )
                $referrer_sort[$site] = array_sum( $urls );
        }
        arsort($referrer_sort);

        return $referrer_sort;
    }

    /**
     * Gets number of clicks as:
     * $dates['all'][Y-n-j] = count
     * $dates['highest']['date'] = Y-n-j
     * $dates['highest']['hits'] = count
     *
     * @param array $hitsbydates
     * @return array
     */
    private function formatDatesArray($hitsbydates)
    {
        /**
         * $hitsbydates looks like:
         * Y-n-j => count
         */

        $highest = array(
            'date' => 'yyyy mm dd',
            'hits' => 0
        );

        foreach ($hitsbydates as $date => $hits)
        {
            if ($hits > $highest['hits']) {
                $highest['date'] = $date;
                $highest['hits'] = $hits;
            }
        }

        krsort($hitsbydates);
        $dates = array (
            'all'     => $hitsbydates,
            'highest' => $highest
        );

        return $dates;
    }

    private function getLastDays($dates)
    {
        $oneweekago  = date('Ymd', strtotime('-1 week'));
        $onemonthago = date('Ymd', strtotime('-30 days'));

        $last_days  = array();
        $last7days  = array();
        $last30days = array();

        $reached7days = false;
        foreach ($dates['all'] as $date => $count)
        {
            if (!$reached7days) {
                if ($date >= $oneweekago) {
                    $last7days[$date] = $count;
                } else {
                    $reached7days = true;
                }
            }
            if ($date >= $onemonthago) {
                $last30days[$date] = $count;
            } else {
                break;
            }
        }

        $last_days['last7days']  = $last7days;
        $last_days['last30days'] = $last30days;

        return $last_days;
    }
}
