<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class Alias extends Model
{
    use Notifiable;

    /**
     * Get all of the alias' flags.
     */
    public function flags()
    {
        return $this->morphMany('App\Flag', 'object');
    }

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'alias';
    }

    public function scopePublic($query)
    {
        return $query->where('hidden', 0)->has('flags', '<', config('app.autoreportthreshold'));
    }

    public function scopeSearch($query, $search)
    {
        if (! $search) return $query;

        return $query->where('url', 'like', "%$search%")
            ->orWhere('alias', 'like', "%$search%");
    }

    public function owners()
    {
        return $this->belongsToMany('App\User', 'permissions');
    }

    public function url()
    {
        return $this->belongsTo('App\Url');
    }

    public function clicks()
    {
        return $this->hasMany('App\Log');
    }

    public function referrers()
    {
        return $this->hasMany('App\Log');
    }

    public function validate()
    {
        $urlValidator = $this->url->validate();

        if ($urlValidator->fails()) {
            return $urlValidator;
        }

        return Validator::make(
            array(
                'alias' => $this->alias
            ),
            array(
                'alias' => ['required', Rule::unique('aliases')->ignore($this->id),
                            Rule::unique('aliases', 'code'),
                            'max:255', new Rules\Alias(), new Rules\Blacklist()]
            )
        );
    }
}
