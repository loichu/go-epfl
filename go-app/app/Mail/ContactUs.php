<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ContactUs extends Mailable
{
    use Queueable, SerializesModels;

    public $sender;
    public $subject;
    public $message;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($sender, $subject, $message)
    {
        $this->sender  = $sender;
        $this->subject = $subject;
        $this->message = $message;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        //dd($this->sender, $this->subject, $this->message);
        return $this->text('emails.contact-us')
                    ->with([
                        'sender'     => $this->sender,
                        'subject'    => $this->subject,
                        'sender_msg' => $this->message
                    ]);
    }
}
