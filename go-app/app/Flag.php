<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FlagType
{
    const REPORT      = 'report';
    const RELIABILITY = 'reliability';
}

class Flag extends Model
{
    /**
     * Get all of the owning flaggable models.
     */
    public function object()
    {
        return $this->morphTo();
    }

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'details' => 'array',
    ];

    public function scopeLinked($query)
    {

    }

    public function scopeType($query, $type)
    {

    }
}
