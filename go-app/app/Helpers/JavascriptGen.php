<?php
/**
 * Created by IntelliJ IDEA.
 * User: loichu
 * Date: 27.03.19
 * Time: 17:14
 */

namespace App\Helpers;

use App\YOURLS\GraphsTrait;

trait JavascriptGen
{
    /**
     * Google charts documentation:
     *   * https://developers.google.com/chart/interactive/docs/datesandtimes
     *
     */

    use GraphsTrait;

    private function javascriptFunction($phpFunction)
    {
        return substr_replace($phpFunction, '', -2);
    }

    private function formatDatesForCharts($dates)
    {
        $returned_dates = array();

        foreach ($dates as $date => $hits)
        {
            $year = substr($date, 0, 4);
            $month = substr($date, 4, 2);
            $day = substr($date, 6, 2);
            if (strlen($date) === 12) {
                $hours = substr($date, 8, 2);
                $mins = substr($date, 10, 2);
            }

            $formatted_date = [$year, $month, $day];
            if (isset($hours)) {
                $formatted_date []= $hours;
                $formatted_date []= $mins;
            }

            $returned_dates[] = array(
                'date' => $formatted_date,
                'hits' => $hits
            );
        }

        return $returned_dates;

    }

    private function chart($type, $javascript_func, $options, $data)
    {
        $js = 'google.charts.setOnLoadCallback('.$javascript_func.');';
        $js.= ' function '.$javascript_func.'() {';
        $js.= ' var data = new google.visualization.DataTable();';

        switch($type) {
            case 'line':
                $js.= $this->lineChart($data['columns'], $data['dates']);
                break;
            case 'pie':
                $js.= $this->pieChart($data);
                break;
            default:
        }

        $js.= ' var options = {';
        foreach ($options as $name => $value)
        {
            $js.= "$name: $value,";
        }
        $js.= '};';

        $js.= " var chart = new google.visualization.".ucfirst($type)."Chart(document.getElementById('stat_".$type."_$javascript_func'));";
        $js.= ' chart.draw(data, options);';
        $js.= '}';

        return $js;
    }

    private function pieChart($data)
    {
        $js = 'data = google.visualization.arrayToDataTable([';
        $js.= '["Country", "Hits"],';
        foreach ($data as $subject => $hits)
        {
            if ($subject)
                $js.= "['$subject', $hits],";
        }
        $js.= ']);';
        return $js;
    }

    private function lineChart($columns, $dates)
    {
        $js = '';

        foreach ($columns as $type => $name)
        {
            $js.= ' data.addColumn(\''.$type.'\', \''.$name.'\');';
        }

        $js.= ' data.addRows([';
        foreach ($dates as $array)
        {
            if (isset($array[3])) {
                list($y, $m, $d, $h, $min) = $array['date'];
            } else {
                list($y, $m, $d) = $array['date'];
            }
            $m = (int) $m - 1; // Date month arg is 0-based (index)
            $d = (int) $d;
            $h = isset($h) ? (int) $h : null;
            $min = isset($min) ? (int) $min : null;
            $date = isset($h)
                ? "new Date(Date.UTC($y, $m, $d, $h, $min))"
                : "new Date(Date.UTC($y, $m, $d))";
            $js.= '['.$date.', '.$array['hits'].'],';
        }
        $js.= ']);';

        return $js;
    }

    private function last24hoursJS($lasthours)
    {
        $javascript_func = $this->javascriptFunction(__FUNCTION__);

        $dates = $this->formatDatesForCharts($lasthours);

        $columns = array(
            'datetime' => 'X',
            'number'    => 'Number of hits'
        );

        $options = array(
            'legend'    => '"none"',
            'hAxis'     => '{title: "Time of day"}',
            'vAxis'     => '{title: "Number of hits"}',
            'curveType' => '"function"'
        );

        return $this->chart('line', $javascript_func, $options, array('dates' => $dates, 'columns' => $columns));
    }

    private function last7daysJS($dates)
    {
        $javascript_func = $this->javascriptFunction(__FUNCTION__);

        $dates = $this->formatDatesForCharts($dates);

        $columns = array(
            'date' => 'X',
            'number'    => 'Number of hits'
        );

        $options = array(
            'legend'    => '"none"',
            'hAxis'     => '{title: "Date"}',
            'vAxis'     => '{title: "Number of hits"}',
            'curveType' => '"function"',
            'width'     => 700,
            'height'    => 300
        );

        return $this->chart('line', $javascript_func, $options, array('dates' => $dates, 'columns' => $columns));
    }

    private function last30daysJS($dates)
    {
        $javascript_func = $this->javascriptFunction(__FUNCTION__);

        $dates = $this->formatDatesForCharts($dates);

        $columns = array(
            'date' => 'X',
            'number'    => 'Number of hits'
        );

        $options = array(
            'legend'    => '"none"',
            'hAxis'     => '{title: "Date"}',
            'vAxis'     => '{title: "Number of hits"}',
            'curveType' => '"function"',
            'width'     => 700,
            'height'    => 300
        );

        return $this->chart('line', $javascript_func, $options, array('dates' => $dates, 'columns' => $columns));
    }

    private function alltimeJS($dates)
    {
        $javascript_func = $this->javascriptFunction(__FUNCTION__);

        $dates = $this->formatDatesForCharts($dates);

        $columns = array(
            'date' => 'X',
            'number'    => 'Number of hits'
        );

        $options = array(
            'legend'    => '"none"',
            'hAxis'     => '{title: "Date"}',
            'vAxis'     => '{title: "Number of hits"}',
            'curveType' => '"function"',
            'width'     => 700,
            'height'    => 300
        );

        return $this->chart('line', $javascript_func, $options, array('dates' => $dates, 'columns' => $columns));
    }

    private function top5JS($countries)
    {
        $javascript_func = $this->javascriptFunction(__FUNCTION__);

        $options = array(
            'width'     => 300,
            'height'    => 300
        );

        return $this->chart('pie', $javascript_func, $options, $countries);
    }

    private function mapJS($countries)
    {
        // https://developers.google.com/chart/image/docs/chart_params#gcharts_chs
        $map = array(
            'cht' => 't',
            'chs' => '440x220',
            'chtm'=> 'world',
            'chco'=> 'FFFFFF,9090AA,202040',
            'chld'=> join('' , array_keys( $countries ) ),
            'chd' => 't:'. join(',' ,  $countries ),
            'chf' => 'bg,s,EAF7FE'
        );
        $map_src = 'http://chart.apis.google.com/chart?' . http_build_query( $map );
        return "<img src='$map_src' witdh='440' height='220' border='0' />";
    }

    private function referrerJS($referrers)
    {
        $javascript_func = $this->javascriptFunction(__FUNCTION__);

        $options = array(
            'width'     => 300,
            'height'    => 300
        );

        $referrers = array(
            'direct'       => $referrers['direct'],
            'by reference' => $referrers['notdirect']
        );

        return $this->chart('pie', $javascript_func, $options, $referrers);
    }
}
