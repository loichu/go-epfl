<?php

namespace App\Rules;

use App\BlacklistItem;
use Illuminate\Contracts\Validation\Rule;

class Blacklist implements Rule
{
    private $matched_keyword;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $blacklist = BlacklistItem::all()->pluck('keyword');
        $blacklist_safe =  true;
        foreach ($blacklist as $keyword)
            if ((bool) preg_match("/$keyword/", $value)) {
                $blacklist_safe = false;
                $this->matched_keyword = $keyword;
                break;
            }
        return $blacklist_safe;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The word "'.$this->matched_keyword.'" is forbidden by blacklist. In case this is a mistake, please use the contact form.';
    }
}
