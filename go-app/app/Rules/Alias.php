<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Log;

class Alias implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $pattern = "/^[a-zA-Z0-9_\-:%\.\?\=\#$\*\+!'(),&;]*$/";

        $curl_url = rawurlencode($value);
        $curl_safe = (bool) preg_match($pattern, $curl_url);

        $html_url = htmlentities($value, ENT_QUOTES, 'UTF-8', false);
        $html_safe = (bool) preg_match($pattern, $html_url);

        return $curl_safe && $html_safe;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The alias :input is not a valid alias';
    }
}
