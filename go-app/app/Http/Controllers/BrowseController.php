<?php namespace App\Http\Controllers;

use App\Alias;
use App\Traits\Pagination;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;
class BrowseController extends Controller
{
    use Pagination;

    public function index()
    {
        $aliases = Alias::public()->withCount('clicks')->paginate(15);
        return $this->displayTable($aliases);
    }

    public function index_datatable()
    {
        $aliases = DB::table('v_aliases')->get();
        //dd($aliases[0]);
        return view('browse-datatable', [
            'aliases' => $aliases,
            'displayed_pages' => 199,
            'page' => 'browse'
        ]);
    }

    public function index_datatable_ajax()
    {
        return view('browse-datatable-ajax');
    }

    public function index_datatable_cache()
    {
        $aliases = Cache::remember('aliases', 600, function () {
            return DB::table('v_aliases')->get();
        });

        return view('browse-datatable', [
            'aliases' => $aliases,
            //'tbody' => $tbody,
            'displayed_pages' => 0,
            'page' => 'browse'
        ]);
    }

    public function search(Request $request)
    {
        $aliases = Alias::public()->withCount('clicks')->leftJoin('urls', function($join){
            $join->on('aliases.url_id', '=', 'urls.id');
        })->search($request->input('q'))->paginate(20);
        return $this->displayTable($aliases);
    }

    public function displayTable($aliases)
    {
        $displayed_pages = $this->pagination($aliases);

        return view('browse', [
            'aliases' => $aliases,
            'displayed_pages' => $displayed_pages,
            'page' => 'browse'
        ]);
    }
}
