<?php

namespace App\Http\Controllers;

use App\Alias;
use App\Log;
use Illuminate\Http\Request;

class Timer {
    public function __construct($name) {
        $this->name = $name;
        $this->started = microtime(true);
    }

    public function stop() {
        $micro = microtime(true) - $this->started;
        print("<!-- Timer " . $this->name ." took $micro s -->");
    }
}

class StatsController extends Controller
{
    public function index()
    {
        $query_timer = new Timer("query");
        //$aliases = Alias::with('url')->get();
      //   $aliases = Alias::leftJoin('urls', function($join) {
      //     $join->on('aliases.url_id', '=', 'urls.id');
      // })->get();
      $aliases = [];
      $query_timer->stop();

        $foreach_timer = new Timer("foreach");
        $urls = array();
        $nb_epfl = 0;
        foreach ($aliases as $alias) {
            // $url = $alias->url;
            $url = "zombo.com";
            if (isset($urls[$url])) {
                $urls[$url] += 1;
            } else {
                $urls[$url] = 1;
                if (preg_match('/epfl.ch/', $url))
                    $nb_epfl += 1;
            }
        }
        $foreach_timer->stop();

        $count_timer = new Timer("count");

        $nb_urls = count($urls);
        $percentage = $nb_urls ? round($nb_epfl / $nb_urls * 100) : 0;
        $nb_aliases = count($aliases);
        $aliases_per_url = $nb_urls ? round(array_sum($urls)/$nb_urls, 3) : 0;
        $most_aliases = empty($urls) ? 'none' : max($urls);
        $most_aliased_url = empty($urls) ? 'none' : array_keys($urls, $most_aliases)[0];
        $nb_redirects = Log::count();

        $count_timer->stop();

        $view_timer = new Timer("view");
        $myview = view('stats', [
            'page'              => 'stats',
            'nb_urls'           => $nb_urls,
            'nb_epfl'           => $nb_epfl,
            'percentage'        => $percentage,
            'nb_aliases'        => $nb_aliases,
            'aliases_per_url'   => $aliases_per_url,
            'most_aliases'      => $most_aliases,
            'most_aliased_url'  => $most_aliased_url,
            'nb_redirects'      => $nb_redirects
        ]);
        $view_timer->stop();

        return $myview;
    }
}
