<?php

namespace App\Http\Controllers\API;

use App\Alias;
use App\Url;
use App\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpFoundation\Response;

class AliasController extends Controller
{
    public function create(Request $request)
    {
        $token = $request->input('token', 'wrong');
        try {
            $user = User::where( 'api_token', $token );
            $user = $user->firstOrFail();
        } catch (ModelNotFoundException $e)
        {
            $message = $token === 'wrong'
                ? __('You need to provide a "token" entry.')
                : __('The "token" you provided is not valid.');

            return \response()->json([
                'status' => 'forbidden',
                'message' => $message
            ], Response::HTTP_FORBIDDEN);
        }
        $url = $request->input('url');
        $aliasInput = $request->input('alias');
        $hidden = $request->has('hidden');
        $user_id = $user->id;

        $url = Url::firstOrCreate(['url' => $url]);
        $alias = new Alias();
        $alias->alias = $aliasInput;
        $alias->url()->associate($url);
        $alias->hidden = $hidden;

        $validator = $alias->validate();
        if ($validator->fails())
        return response()->json([
            'status' => 'invalid',
            'errors' => $validator->errors()
        ], Response::HTTP_UNPROCESSABLE_ENTITY);

        $alias->save();
        $alias->owners()->attach($user_id);

        return response()->json([
            'status' => 'created',
            'url'    => config('app.url').$alias->alias
        ], Response::HTTP_CREATED);
    }
}
