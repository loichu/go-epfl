<?php

namespace App\Http\Controllers;

use App\Alias;
use App\Flag;
use App\FlagType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FlagsController extends Controller
{
    public function report(Alias $alias)
    {
        $flag = new Flag;
        $flag->flag_type = FlagType::REPORT;
        $flag->details = array(
            'sciper'  => Auth::user()->sciper,
            'email'   => Auth::user()->email,
            'message' => 'Inappropriate'
        );

        $alias->flags()->save($flag);

        return redirect('reportconfirm')->with('alias', $alias);
    }
}
