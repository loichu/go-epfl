<?php

namespace App\Http\Controllers;

use DateTime;
use Illuminate\Http\Request;
use App\Url;
use App\Alias;
use App\Rules;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
class AliasController extends Controller
{
    public function create(Request $request)
    {
        $aliasInput      = $request->input('alias');
        $urlInput        = $request->input('url');
        $hiddenAlias     = $request->has('hidden');
        $timeToLive      = $request->input('ttl');

        $user_id = Auth::id();

        $url   = Url::firstOrCreate(['url' => $urlInput]);
        $alias = new Alias;
        $alias->alias = $aliasInput;
        $alias->url()->associate($url);
        $alias->hidden = $hiddenAlias;
        if ($timeToLive > 0)
            $alias->obsolescence_date = date('Y-m-d', strtotime('+'.$timeToLive.' months'));

        $validator = $alias->validate();
        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        $alias->save();
        $alias->owners()->attach($user_id);

        return view('aliasconfirm', ['new_url' => config('app.url').$aliasInput, 'alias' => $alias, 'page' => 'home']);
    }

    public function aliasObject($alias) : Alias
    {
        if ($alias = Alias::where(['alias' => $alias])->first())
            return $alias;
        else
            return new Alias();
    }

    public function aliasEmpty(Alias $alias) : bool
    {
        if ($alias->is(new Alias()))
            return true;
        else
            return false;

    }

    public function userOwns(Alias $alias)
    {
        return $alias->owners->contains(Auth::user());
    }

    public function edit($alias)
    {
        if ($this->aliasEmpty( $alias = $this->aliasObject($alias) ))
            abort(404);

        $obsolescence_date = new DateTime($alias->obsolescence_date);

        return $this->userOwns($alias) ? view('edit.alias', [
            'alias'             => $alias->alias,
            'url'               => $alias->url->url,
            'hidden'            => $alias->hidden,
            'obsolescence_date' => $obsolescence_date->format('Y-m-d'),
            'ttl'               => $alias->obsolescence_date ?: 0,
        ]) : abort(403, 'You are not allowed to edit this alias');

    }

    public function update(Request $request)
    {
        $old_alias = $request->input('original_alias');
        $alias = $this->aliasObject($old_alias);
        if ($this->aliasEmpty($alias))
            abort(404);

        if (!$this->userOwns($alias))
            abort(403, 'You are not allowed to edit this alias');

        $hiddenAlias = $request->has('hidden');
        $alias->hidden = $hiddenAlias;
        $alias->alias = $request->input('alias');
        $timeToLive = $request->input('ttl');
        if ($timeToLive > 0) {
            $alias->obsolescence_date = date('Y-m-d', strtotime('+'.$timeToLive.' months'));
        } else {
            $alias->obsolescence_date = null;
        }

        $url = Url::firstOrCreate(['url' => $request->input('url')]);
        $alias->url()->associate($url);

        $validator = $alias->validate();
        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }
        $alias->save();
        return redirect('profile')->with('status', 'The alias has been updated');
    }
}
