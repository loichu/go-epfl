<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Alias;
use App\Log;

class RedirectController extends Controller
{
    private $request;
    private $alias;

    public function __invoke(Request $request, Alias $alias)
    {
        if($alias->obsolete)
            abort(404);

        $this->request = $request;
        $this->alias   = $alias;

        $user_agent = $request->server('HTTP_USER_AGENT');
        $ip = $request->server('HTTP_X_REAL_IP') ?: "0.0.0.0";
        $country_iso_code = geoip_country_code_by_name($ip);
        $referrer = $request->server('HTTP_REFERER') ?: 'direct';

        $log = Log::create([
            'click_time' => date('Y-m-d H:i'),
            'referrer'   => $referrer,
            'user_agent' => $user_agent,
            'ip_address' => $ip,
            'iso_code'   => $country_iso_code,
            'alias_id'   => $alias->id,
            'alias'      => $alias->alias,
            'url'        => $alias->url['url'],
        ]);

        if (count($alias->flags) > 0) {
            return $this->reveal('This alias has been reported!');
        } elseif (count($alias->url->flags) > 0) {
            return $this->reveal('This URL may not be accessible');
        } else {
            return redirect($alias->url['url']);
        }
    }

    public function reveal($message)
    {
        $this->request->session()->flash(
            'message', $message
        );
        return view('reveal', [
            'alias' => $this->alias->alias,
            'url'   => $this->alias->url['url']
        ]);
    }
}
