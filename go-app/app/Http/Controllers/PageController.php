<?php

namespace App\Http\Controllers;

use App\Mail\ContactUs;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class PageController extends Controller
{
    public function FAQ()
    {
        return view('faq', ['page' => __FUNCTION__]);
    }

    public function about()
    {
        return view('about', ['page' => __FUNCTION__]);
    }

    public function contact()
    {
        return view('contact', ['page' => __FUNCTION__]);
    }

    public function api()
    {
        return view('api', ['page' => __FUNCTION__]);
    }

    public function revealInfo()
    {
        return view('reveal-info', ['page' => __FUNCTION__]);
    }

    public function contactSend(Request $request)
    {
        $sender     = Auth::user()->username ?? 'guest';
        $subect     = $request->input('subject');
        $message    = $request->input('message');
        $recipient  = $request->input('recipient');
        $to = '';

        if ($recipient === 'bug')
            $to = 'incoming+epfl-idevfsd-go-epfl-10607222-issue-@incoming.gitlab.com';
        elseif ($recipient === 'admin')
            $to = 'go-dev@groupes.epfl.ch';

        Mail::to($to)
            ->send(new ContactUs($sender, $subect, $message));

        return view('mailconfirm');
    }
}
