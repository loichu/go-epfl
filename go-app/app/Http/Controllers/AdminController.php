<?php

namespace App\Http\Controllers;

use App\Alias;
use App\BlacklistItem;
use App\Flag;
use App\Traits\Pagination;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;

class AdminController extends Controller
{
    use Pagination;

    public function index()
    {
        return view('admin.home', ['page' => 'Admin', 'sub' => 'none']);
    }

    public function aliases()
    {
        $aliases = Alias::with(['url', 'owners', 'flags']);
        $owners = array();
        $flags = array();
        foreach ($aliases->get() as $alias)
        {
            foreach ($alias->owners as $owner)
                $owners[$alias->alias][] = $owner->email;
            foreach ($alias->flags as $flag)
                $this->increment($flags[$alias->alias], $flag->flag_type);

        }
        $aliases = $aliases->paginate();
        return view('admin.aliases', [
            'page' => 'Admin',
            'sub'  => 'Aliases',
            'displayed_pages' => $this->pagination($aliases),
            'aliases' => $aliases,
            'owners' => $owners,
            'flags' => $flags
        ]);
    }

    public function editAlias(Alias $alias)
    {
        $owners = array();
        foreach ($alias->owners as $owner)
            $owners []= $owner->email;

        return view('admin.edit_alias', [
            'page'              => 'Admin',
            'sub'               => 'Aliases',
            'alias'             => $alias->alias,
            'url'               => $alias->url->url,
            'obsolescence_date' => $alias->obsolescence_date,
            'ttl'               => $alias->obsolescence_date ?: 0,
            'hidden'            => $alias->hidden,
            'owners'            => $owners
        ]);
    }

    public function deleteAlias(Alias $alias)
    {
        $success_message = "Alias \"$alias->alias\" successfully deleted from blacklist";
        $alias->delete();
        Session::flash('status', $success_message);
        return $success_message;
    }

    public function updateOwner(Request $request)
    {
        $owner_email = $request->input('new_owner');
        $alias = Alias::where('alias', $request->input('alias'))->first();
        if (is_null($user = User::where('email', $owner_email)->first()))
            return redirect()->back()->withErrors("The user with email \"$owner_email\" does not exist");
        $alias->owners()->attach($user);
        $alias->save();
        return redirect()->back()->with('status', "$user->username has been added to $alias->alias's owners");
    }

    public function flags()
    {
        $_reports = Flag::with('object')
            ->where('flag_type', 'report')->get();
        $reports = array();
        foreach ($_reports as $report)
            if (!empty($report->object))
                $this->increment($reports, $report->object['alias']);

        $_unaccessible = Flag::with('object')
            ->where('flag_type', 'reliability')->get();
        $unaccessible = array();
        foreach ($_unaccessible as $unreliable)
            if (!empty($unreliable->object))
                $unaccessible[$unreliable->object['url']] = $unreliable;

        $_blacklist = Flag::with('object')
            ->where('flag_type', 'blacklist')->get();
        $blacklist = array();
        foreach ($_blacklist as $item)
            if (!empty($item->object))
                if (isset($item->object['url']))
                    $this->increment($blacklist, $item->object['url']);
                elseif (isset($item->object['alias']))
                    $this->increment($blacklist, $item->object['alias']);

        return view('admin.flags', [
            'page'          => 'Admin',
            'sub'           => 'Flags',
            'reports'       => $reports,
            'unaccessible'  => $unaccessible,
            'blacklist'     => $blacklist,
        ]);
    }

    private function increment(&$array, $key)
    {
        if(!isset($array[$key]))
            $array[$key] = 1;
        else
            $array[$key] += 1;
    }

    public function people()
    {
        $people = User::withCount(['aliases'])->paginate();
        return view('admin.people', [
            'page' => 'Admin',
            'sub'  => 'People',
            'displayed_pages' => $this->pagination($people),
            'people' => $people,
        ]);
    }

    public function blacklist()
    {
        $blacklist = BlacklistItem::pluck('keyword')->all();
        return view('admin.blacklist', [
            'page' => 'Admin',
            'sub'  => 'Blacklist',
            'blacklist' => $blacklist
        ]);
    }

    public function appendToBlacklist(Request $request)
    {
        $validated = $request->validate([
            'keyword' => 'required|unique:blacklist_items|max:255'
        ]);
        $word = new BlacklistItem();
        $word->keyword = $validated['keyword'];
        $word->save();
        return redirect()->back()
                         ->with('status', "Keyword \"$word->keyword\" successfully added to blacklist");
    }

    public function deleteFromBlacklist(BlacklistItem $keyword)
    {
        $success_message = "Keyword \"$keyword->keyword\" successfully deleted from blacklist";
        $keyword->delete();
        Session::flash('status', $success_message);
        return $success_message;
    }

    public function advanced()
    {
        $administrators = User::admin()->get();
        return view('admin.advanced', [
            'page' => 'Admin',
            'sub'  => 'Advanced',
            'administrators' => $administrators
        ]);
    }

    public function addAdministrator(Request $request)
    {
        $new_admin_email = $request->input('new_admin');
        $new_admin       = User::where('email', $new_admin_email)->first();
        if (empty($new_admin))
            return redirect()->back()->withErrors(
                "There is no user with the email address \"$new_admin_email\""
            );
        $new_admin->is_admin = true;
        $new_admin->save();
        return redirect()->back()->with('status', "$new_admin->username is now administrator");
    }

    public function runReliabilityTest()
    {
        dispatch(function () {
            $reliability_test = new \App\Console\ReliabilityTest;
            $reliability_test();
        })->onQueue('reliability_test');
        return redirect()->back()->with('status', 'Reliability test started');
    }
}
