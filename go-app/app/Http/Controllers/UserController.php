<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    private $user;

    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function generateToken()
    {
        $token = $this->getToken(16);
        while(User::where('api_token', $token)->exists())
            $token = $this->getToken(16);
        Auth::user()->api_token = $token;
        Auth::user()->save();
        return redirect('profile');
    }

    public function deleteToken()
    {
        Auth::user()->api_token = null;
        Auth::user()->save();
        return redirect('profile');
    }

    public function details()
    {
        $aliases = Auth::user()->aliases()->get();

        return view('user', [
            'user'    => Auth::user(),
            'aliases' => $aliases
        ]);
    }

    private function getToken($length)
    {
        $token = "";
        $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $codeAlphabet.= "abcdefghijklmnopqrstuvwxyz";
        $codeAlphabet.= "0123456789";
        $max = strlen($codeAlphabet); // edited

        for ($i=0; $i < $length; $i++) {
            $token .= $codeAlphabet[rand(0, $max-1)];
        }

        return $token;
    }
}
