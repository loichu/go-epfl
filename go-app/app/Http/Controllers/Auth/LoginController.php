<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /**
     * Redirect the user to the Tequila authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider()
    {
        return Socialite::driver('tequila')->redirect();
    }

    /**
     * Obtain the user information from Tequila.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback()
    {
        $user = Socialite::driver('tequila')->user();

        $auth_user = User::updateOrCreate(
            [
                'email'  => $user->email,
                'sciper' => $user->sciper
            ],
            [
                'token'     => $user->token,
                'firstname' => $user->firstname,
                'lastname'  => $user->name,
                'username'  => $user->username,
            ]
        );
        Auth::login($auth_user, true);

        return redirect()->to('/');
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->back();
    }
}
