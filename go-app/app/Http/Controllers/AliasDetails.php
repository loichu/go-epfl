<?php

namespace App\Http\Controllers;

use App\Alias;
use App\Helpers\JavascriptGen;
use Illuminate\Support\Facades\DB;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class AliasDetails extends Controller
{
    use JavascriptGen;

    private $alias;

    public function info($alias)
    {
        if (!$alias = Alias
            ::where(['alias' => $alias])
            ->withCount('clicks')
            ->first())
        {
            abort(404);
        }

        $this->alias = $alias;
        $qrcode = $this->QRcode();

        if ($alias->clicks_count == 0)
            return view('info-nostats', [
                'alias'  => $alias,
                'qrcode' => $qrcode
            ]);



        $rec_countries = $this->ReqCountries();
        $countries = array(
            'all'  => $rec_countries,
            'top5' => array_slice($rec_countries, 0, 5)
        );
        $detailed_countries = $this->getCountriesDetails($countries['top5']);

        $referrers = $this->getReferrers($alias->id);
        $referrers_sort = $this->getSortedReferrers($referrers);

        $dates = $this->formatDatesArray($this->ReqHitsByDay());
        $dates['highest']['date'] = date( "l d F Y",
            strtotime(
                $dates['highest']['date']
            )
        );
        $lasthours = $this->ReqHitsLastHours();

        return view('info', [
            'alias'             => $alias,
            'qrcode'            => $qrcode,
            'charts'            => $this->Charts(
                $lasthours,
                $dates,
                $countries,
                $referrers
            ),
            'countries'         => $detailed_countries,
            'best_day'          => $dates['highest'],
            'referrers'         => $referrers,
            'referrer_sort'     => $referrers_sort,
            'page'              => 'browse',
        ]);
    }

    private function Charts($lasthours, $dates, $countries, $referrers)
    {
        $charts = array();

        $last_days = $this->getLastDays($dates);

        $charts['hits'] = array (
            'last24hours' => array(
                'label' => 'Last 24 hours',
                'js'    => $this->last24hoursJS($lasthours)
            ),
            'last7days'   => array(
                'label' => 'Last 7 days',
                'js'    => $this->last7daysJS($last_days['last7days'])
            ),
            'last30days'  => array(
                'label' => 'Last 30 days',
                'js'    => $this->last30daysJS($last_days['last30days'])
            ),
            'alltime'     => array(
                'label' => 'All time',
                'js'    => $this->alltimeJS($dates['all'])
            ),
        );

        $charts['countries'] = array (
            'top5' => $this->top5JS($countries['top5']),
            'map'  => $this->mapJS($countries['all']),
        );

        $charts['referrers']['directVStraffic'] = $this->referrerJS($referrers);

        return $charts;
    }

    private function QRcode()
    {
        return base64_encode(QrCode::format('png')
            ->size(150)
            ->margin(0)
            ->encoding('UTF-8')
            ->generate(config('app.url').$this->alias->alias));
    }

    private function ReqHitsByDay()
    {
        $records = DB::table('logs')
            ->selectRaw('EXTRACT(YEAR FROM click_time) as year,
                        EXTRACT(MONTH FROM click_time) as month,
                        EXTRACT(DAY FROM click_time)  as day,
                        COUNT(*) as count')
            ->where(['alias_id' => $this->alias->id])
            ->groupBy(['year', 'month', 'day'])
            ->get();

        $hitsbyday = array();
        foreach ($records as $rec)
        {
            $y = $rec->year;
            $m = $rec->month;
            $d = $rec->day;
            $date = date('Ymd', strtotime($y.'-'.$m.'-'.$d));
            $hitsbyday[$date] = $rec->count;
        }

        return $hitsbyday;
    }

    private function ReqHitsLastHours()
    {
        $records = DB::table('logs')
            ->selectRaw('EXTRACT(hour FROM click_time) as hour,
                        EXTRACT(minute FROM click_time) as minute,
                        EXTRACT(day FROM click_time) as day,
                        EXTRACT(month FROM click_time) as month,
                        EXTRACT(year FROM click_time) as year,
                        COUNT(*) as count')
            ->whereRaw(
                'alias_id = '.$this->alias->id.
                ' AND click_time >= (now() - interval \'24 hours\')'
            )
            ->groupBy(['year', 'month', 'day', 'hour', 'minute'])
            ->get();

        $hitslasthours = array();
        foreach ($records as $rec) {
            $y = $rec->year;
            $m = $rec->month;
            $d = $rec->day;
            $h = $rec->hour;
            $mn = $rec->minute;
            $time = date('YmdHi', strtotime($y.'-'.$m.'-'.$d.' '.$h.':'.$mn));
            $hitslasthours[$time] = $rec->count;
        }

        return $hitslasthours;
    }

    private function ReqCountries()
    {
        $country_rows = DB::table('logs')
            ->select('iso_code', DB::Raw('count (*) as count'))
            ->where(['alias_id' => $this->alias->id])
            ->groupBy('iso_code')
            ->get();

        // Loop through all results and build list of countries and hits
        $countries = array();
        foreach( $country_rows as $row ) {
            if (isset($row->iso_code))
                $countries[$row->iso_code] = $row->count;
        }

        // Sort countries, most frequent first
        if ( isset($countries) )
            arsort( $countries );

        return $countries;
    }
}
