<?php

namespace App\Http\Controllers;

use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;
use App\Alias;

/**
 * This class create a Atom Syndication
 * See https://tools.ietf.org/html/rfc4287
 * See https://validator.w3.org/feed/docs/atom.html
 */
class FeedController extends Controller
{
    public $feed;

    public function index()
    {
        // Define the feed metadata
        $this->getFeedMetadata();
        $this->getFeedEntries();
        // Get the view's content
        $content = view('layouts.feed', ['feed' => $this->feed]);
        // Return the HTTP response with the content specifying the header's 
        // Content-Type to text/xml 
        // (see https://stackoverflow.com/a/3272572/960623)
        return response($content, 200)
                ->header('Content-Type', 'text/xml');
    }
    
    /**
     * Set the metadata elements of the feed
     **/
    private function getFeedMetadata()
    {
        $this->feed                = (object)[];
        $this->feed->title         = 'Go.epfl.ch\'s syndication feed';
        $this->feed->link          = 'https://go.epfl.ch/feed';
        $this->feed->updated       = date('c');
        $this->feed->author        = (object)[];
        $this->feed->author->name  = 'Go.epfl.ch\'s admins';
        $this->feed->author->email = 'go@groupes.epfl.ch';
        $this->feed->author->uri   = 'https://go.epfl.ch/contact';
        $this->feed->id            = 'urn:uuid:7d90b9a3-b584-4aae-ae76-db1b5823cf82';
    }
    
    /**
     * Set the entries elements of the feed
     **/
    private function getFeedEntries()
    {
        $this->feed->entries = Alias::public()
                                ->withCount('clicks')
                                ->leftJoin('urls', 'aliases.url_id', '=', 'urls.id')
                                ->orderby('aliases.created_at', 'DESC')
                                ->limit(30)
                                ->get();
    }
}