<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AliasResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'alias' => $this->alias,
            'url' => $this->url->url,
            'clicks' => $this->clicks_count,
            'owners' => $this->owners(),
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }

    private function owners()
    {
        $owners = array();
        foreach ($this->owners as $owner)
        {
            $owners []= $owner->email;
        }
        return $owners;
    }
}
