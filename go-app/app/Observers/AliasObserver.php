<?php

namespace App\Observers;

use App\Alias;

class AliasObserver
{
    /**
     * Handle the alias "retrieved" event.
     *
     * @param  \App\Alias  $alias
     * @return void
     */
    public function retrieved(Alias $alias)
    {
        if (isset($alias->obsolescence_date) && date('Y-m-d H:i') >= $alias->obsolescence_date)
        {
            $alias->obsolete = true;
            $alias->save();
        }
    }
}
