<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class Url extends Model
{
    protected $fillable = ['url'];

    /**
     * Get all of the url's flags.
     */
    public function flags()
    {
        return $this->morphMany('App\Flag', 'object');
    }

    public function aliases()
    {
        return $this->hasMany('App\Alias');
    }

    public function validate()
    {
        return Validator::make(
            array(
                'url'   => $this->url
            ),
            array(
                'url'   => ['required', new Rules\URL(), new Rules\Blacklist()]
            )
        );
    }
}
