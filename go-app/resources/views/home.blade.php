@extends('layouts.app')
@section('content')
    <h3 class="tlbx-variant-heading">GO EPFL - URL shortener</h3>
    @auth
    <br />
    <h5>Shorten a new URL</h5>

    @include('partials.edit_alias', [
        'prev_url'      => old('url') ?? '',
        'prev_alias'    => old('alias') ?? '',
        'hidden'        => false,
        'ttl'           => 0,
        'submitButton'  => 'Shorten URL'
    ])

    @endauth

    @guest
    <br />
    <h3>Info</h3>
    <p>
        <a href="https://go.epfl.ch">go.epfl.ch</a> is a service offered to the
        EPFL community to shorten URLs.
        <br />
        <br />
        <a href="/login" class="btn btn-info" role="button">Login</a> is required in order to create new shortened URLs,
        but other pages of this website (<a href="{{ route('about') }}">{{ __('About') }}</a>,
        <a href="{{ route('stats') }}">{{ __('Stats') }}</a>, <a href="{{ route('FAQ') }}">{{ __('FAQ') }}</a>, ...)
        can be reached as an unauthenticaed user.
    </p>

    <br />
    <h3>What is it?</h3>
    <p>
        URL shortening is a technique in which a URL may be made substantially
        shorter in length and have (sometimes) more meaning.<br />
        <h5>Example:</h5>
        <a href="https://go.epfl.ch/mag">https://www.epfl.ch/campus/services/communication/audiences-canaux/epfl-magazine/</a> becomes <a href="https://go.epfl.ch/mag">https://go.epfl.ch/mag</a>.
    </p>

    <br />
    <h3>Reveal URLs</h3>
    <p>
        If you want to know where a shortened URL will redirect, you can find it out with the URL <code>/reveal/alias</code>.
        <br />For instance, to know where <a href="https://go.epfl.ch/mag">https://go.epfl.ch/<b>mag</b></a> will redirect, use <a href="https://go.epfl.ch/reveal/mag">https://go.epfl.ch/<b>reveal</b>/mag</a>.
    </p>
    @endguest
@endsection
