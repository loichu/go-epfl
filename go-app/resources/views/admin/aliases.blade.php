@extends('admin.layout')

@section('admin-content')
    <h3 class="tlbx-variant-heading">Manage aliases</h3>
    <br />
    <table id="aliases" class="table table-boxed dataTable" >
        <thead>
            <tr>
                <th>Alias</th>
                <th>URL</th>
                <th>{{__('Owner(s)')}}</th>
                <th>{{__('Flags')}}</th>
                <th class="no-sort">Actions</th>
            </tr>
        </thead>
        <tbody>
        @foreach($aliases as $alias)
            <tr>
                <td><a href="/{{{$alias->alias}}}">{{{$alias->alias}}}</a></td>
                <td><a href="/{{{$alias->alias}}}">{{{$alias->url->url}}}</a></td>
                <td>
                    @isset($owners[$alias->alias])
                        {{implode(', ', $owners[$alias->alias])}}
                    @endisset
                </td>
                <td>
                    @isset($flags[$alias->alias])
                        @foreach ($flags[$alias->alias] as $flag => $qty)
                            @if ($flag === 'report')
                                <span class="badge badge-pill badge-danger">{{$flag}} ({{$qty}})</span>
                            @elseif ($flag === 'reliability')
                                <span class="badge badge-pill badge-secondary">{{$flag}} ({{$qty}})</span>
                            @elseif ($flag === 'blacklist')
                                <span class="badge badge-pill badge-warning">{{$flag}} ({{$qty}})</span>
                            @endif
                        @endforeach
                    @endisset
                </td>
                <td style="text-align: center">
                    <a href="/admin/alias/{{$alias->alias}}">edit</a>
                    <a href="/admin/alias/delete/{{$alias->alias}}" class="jquery-postback" data-method="delete">
                        delete
                    </a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
<!--
    <nav aria-label="Page navigation">
        <ul class="pagination justify-content-center">
            @if(!empty($displayed_pages['before']))
                <li class="page-item">
                    <a class="page-link" href="{{$aliases->url(1)}}" aria-label="Go to first page">
                        <svg class="icon" aria-hidden="true"><use xlink:href="#icon-chevron-last-left"></use></svg>
                    </a>
                </li>
                <li class="page-item">
                    <a class="page-link" href="{{$aliases->previousPageUrl()}}" aria-label="Go to previous page">
                        <svg class="icon" aria-hidden="true"><use xlink:href="#icon-chevron-left"></use></svg>
                        <span class="sr-only">Previous</span>
                    </a>
                </li>
                @foreach($displayed_pages['before'] as $number => $url)
                    <li class="page-item">
                        <a class="page-link" href="{{$url}}" aria-current="page">
                            {{$number}}
                        </a>
                    </li>
                @endforeach
            @endif
            <li class="page-item active">
                <a class="page-link" href="#" aria-current="page">
                    {{$aliases->currentPage()}}
                    <span class="sr-only">(Current page)</span>
                </a>
            </li>
            @if(!empty($displayed_pages['after']))
                @foreach($displayed_pages['after'] as $number => $url)
                    <li class="page-item">
                        <a class="page-link" href="{{$url}}" aria-current="page">
                            {{$number}}
                        </a>
                    </li>
                @endforeach
                <li class="page-item">
                    <a class="page-link" href="{{$aliases->nextPageUrl()}}" aria-label="Go to next page">
                        <svg class="icon" aria-hidden="true"><use xlink:href="#icon-chevron-right"></use></svg>
                        <span class="sr-only">Previous</span>
                    </a>
                </li>
                <li class="page-item">
                    <a class="page-link" href="{{$aliases->url($aliases->lastPage())}}" aria-label="Go to last page">
                        <svg class="icon" aria-hidden="true"><use xlink:href="#icon-chevron-last-right"></use></svg>
                    </a>
                </li>
            @endif
        </ul>
    </nav> -->
@endsection
