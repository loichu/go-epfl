@extends('admin.layout')

@section('admin-content')
    <h3 class="tlbx-variant-heading">Manage flags 🚩</h3>
    <br />
    <div class="row">
        <div class="col-xl-4">
            <h5>Reports</h5>
            <table class="table dataTableSimple">
                <thead>
                    <tr>
                        <th>Alias</th>
                        <th>Reports count</th>
                        <th class="no-sort">Details</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($reports as $alias => $reports_count)
                    <tr>
                        <td>{{$alias}}</td>
                        <td>{{$reports_count}} 🚩</td>
                        <td><a href="/reports/{{$alias}}">details</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="col-xl-4">
            <h5>Unreliable</h5>
            <table class="table">
                <tbody>
                @foreach($unaccessible as $url => $unreliable)
                    <tr>
                        <td>{{$url}}</td>
                        <td>{{$unreliable->details['status_code']}}</td>
                        <td><a href="/unreliable/{{$unreliable->id}}">details</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="col-xl-4">
            <h5>Blacklist</h5>
            <table class="table">
                <tbody>
                @foreach($blacklist as $object => $blacklist_count)
                    <tr>
                        <td>
                            {{$object}} ({{$blacklist_count}})
                        </td>
                        <td><a href="/blacklist/{{$blacklisted->id}}">details</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
