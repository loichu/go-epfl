@extends('admin.layout')
@section('admin-content')
    <h3 class="tlbx-variant-heading">Backoffice</h3>
    @auth
    <br />
    [WIP]
    <h5>Actions</h5>
    <p>This is a list of idea of kind of action administrators might have to do:
    <ul>
        <li><a href="#">💡 See reported URL</a></li>
        <li><a href="#">💡 See URLs that return 404</a></li>
        <li><a href="#">💡 Resolve claimed URL</a></li>
        <li><a href="#">💡 See URLs submitted by a user</a></li>
        <li><a href="#">💡 Transfer authority of user's URLs to another one</a></li>
        <li><a href="#">💡 ...</a></li>
    </ul>
    @endauth
    @guest
    <span>Only admins are allowed here. Please <a href="/login">login</a> if you are granted here.</span>
    @endguest
@endsection
