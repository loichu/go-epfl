@extends('layouts.skeleton')
@section('body')
    <div class="container-fluid">
        @include('layouts.errors')
        <div class="row">
            <div class="col-md-3">
                <nav
                    id="nav-aside"
                    class="nav-aside"
                    role="navigation"
                    aria-describedby="nav-aside-title"
                >
                    <h2 class="h5 sr-only-xl">{{__('Submenu')}}</h2>
                    <ul>
                        <li class="{{$sub === 'Aliases' ? 'active' : ''}}">
                            <a href="/admin/aliases">{{__('Aliases')}}</a></li>
                        <li class="{{$sub === 'Flags' ? 'active' : ''}}">
                            <a href="/admin/flags">{{__('Flags')}}</a></li>
                        <li class="{{$sub === 'People' ? 'active' : ''}}">
                            <a href="/admin/people">{{__('People')}}</a></li>
                        <li class="{{$sub === 'Blacklist' ? 'active' : ''}}">
                            <a href="/admin/blacklist">{{__('Blacklist')}}</a></li>
                        <li class="{{$sub === 'Advanced' ? 'active' : ''}}">
                            <a href="/admin/advanced">{{__('Advanced')}}</a></li>
                    </ul>
                </nav>
            </div>
            <div class="col-md-9">
                @yield('admin-content')
            </div>
        </div>
    </div>
@endsection

@section('javascript')
<script>
    $(function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $(document).on('click', 'a.jquery-postback', function (e) {
            e.preventDefault(); // does not go through with the link.

            var $this = $(this);

            $.post({
                type: $this.data('method'),
                url: $this.attr('href')
            }).done(function (data) {
                console.log(data);
                location.reload();
            });
        });
    })
</script>
@endsection
