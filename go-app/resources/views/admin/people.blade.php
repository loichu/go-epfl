@extends('admin.layout')

@section('admin-content')
    <h3 class="tlbx-variant-heading">Manage users</h3>
    <br />
    <table id="people" class="table dataTable table-boxed" >
        <thead>
            <tr>
                <th>Email</th>
                <th>Aliases count</th>
                <th class="no-sort">API Token</th>
            </tr>
        </thead>
        <tbody>
        @foreach($people as $user)
            <tr>
                <td>{{$user->email}}</td>
                <td>{{$user->aliases_count}}</td>
                <td style="text-align: center">
                    @isset($user->api_token)
                        <a href="#">revoke</a>
                        <a href="#">regenerate</a>
                    @else
                        <a href="#">generate</a>
                    @endisset
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
<!--
    <nav aria-label="Page navigation">
        <ul class="pagination justify-content-center">
            @if(!empty($displayed_pages['before']))
                <li class="page-item">
                    <a class="page-link" href="{{$people->url(1)}}" aria-label="Go to first page">
                        <svg class="icon" aria-hidden="true"><use xlink:href="#icon-chevron-last-left"></use></svg>
                    </a>
                </li>
                <li class="page-item">
                    <a class="page-link" href="{{$people->previousPageUrl()}}" aria-label="Go to previous page">
                        <svg class="icon" aria-hidden="true"><use xlink:href="#icon-chevron-left"></use></svg>
                        <span class="sr-only">Previous</span>
                    </a>
                </li>
                @foreach($displayed_pages['before'] as $number => $url)
                    <li class="page-item">
                        <a class="page-link" href="{{$url}}" aria-current="page">
                            {{$number}}
                        </a>
                    </li>
                @endforeach
            @endif
            <li class="page-item active">
                <a class="page-link" href="#" aria-current="page">
                    {{$people->currentPage()}}
                    <span class="sr-only">(Current page)</span>
                </a>
            </li>
            @if(!empty($displayed_pages['after']))
                @foreach($displayed_pages['after'] as $number => $url)
                    <li class="page-item">
                        <a class="page-link" href="{{$url}}" aria-current="page">
                            {{$number}}
                        </a>
                    </li>
                @endforeach
                <li class="page-item">
                    <a class="page-link" href="{{$people->nextPageUrl()}}" aria-label="Go to next page">
                        <svg class="icon" aria-hidden="true"><use xlink:href="#icon-chevron-right"></use></svg>
                        <span class="sr-only">Previous</span>
                    </a>
                </li>
                <li class="page-item">
                    <a class="page-link" href="{{$people->url($aliases->lastPage())}}" aria-label="Go to last page">
                        <svg class="icon" aria-hidden="true"><use xlink:href="#icon-chevron-last-right"></use></svg>
                    </a>
                </li>
            @endif
        </ul>
    </nav> -->
@endsection
