@extends('layouts.app')

@section('content')
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script>
    /* Load the Visualization API and the corechart package. */
    google.charts.load('current', {'packages':['corechart']});
    </script>

    <h3 class="tlbx-variant-heading">Info</h3>
    <div class="row">
        <div class="col-md-8"  id="total_hits">
            <p><b><a href="{{config('app.url').$alias->alias}}">{{config('app.url').$alias->alias}}</a></b></p>
            <p>
                {{__('Total hits')}}: {{$alias->clicks_count}}
            </p>
        </div>
        <div class="col-md-4"  id="qrcode-container">
            <h3>QR Code</h3>
            <img id="qrcode" src="data:img/png;base64, {!! $qrcode !!}" alt="qrcode for {{$alias->url->url}}"/>
        </div>
    </div>

    <div id="tabs">
    <div class="wrap_unfloat">
        <ul id="headers" class="toggle_display stat_tab">
            <li class="selected"><a href="#stat_tab_stats"><h2>{{__( 'Traffic statistics')}}</h2></a></li>
            <li><a href="#stat_tab_location"><h2>{{__( 'Traffic location')}}</h2></a></li>
            <li><a href="#stat_tab_sources"><h2>{{__( 'Traffic sources')}}</h2></a></li>
        </ul>
    </div>

    <div id="stat_tab_stats" class="tab">
        <h2>{{__( 'Traffic statistics')}}</h2>
        <table border="0" cellspacing="2">
            <tr>
                <td valign="top">
                    <ul id="stats_lines" class="toggle_display stat_line">
                        <li><a href="#stat_line_last24hours" class="" style="text-decoration:none">{{__( 'Last 24 hours' )}}</a></li>
                        <li><a href="#stat_line_last7days">{{__( 'Last 7 days' )}}</a></li>
                        <li><a href="#stat_line_last30days">{{__( 'Last 30 days' )}}</a></li>
                        <li><a href="#stat_line_alltime">{{__( 'All time' )}}</a></li>
                    </ul>
                    @foreach($charts['hits'] as $id => $graph)
                        <script>
                            {!! $graph['js'] !!}
                        </script>
                        <div id='stat_line_{{$id}}' class='stats_line line' style='display:block; width:700px; height:300px'>
                            <h3>{{$graph['label']}}</h3>
                        </div>
                    @endforeach
                </td>
                <td valign="top">
                    <h3>{{__( 'Historical click count' )}}</h3>

                    <p>{{sprintf('Short URL created on %s', $alias->created_at)}}</p>

                    <h3>{{__( 'Best day' )}}</h3>
                    <p>
                        {!! sprintf('<strong>%1$s</strong> hits on %2$s', $best_day['hits'], $best_day['date']) !!}
                    </p>
                </td>
            </tr>
        </table>
    </div>


    <div id="stat_tab_location" class="tab">
        <h2>{{__( 'Traffic location' )}}</h2>

        @if ( $countries )

            <table border="0" cellspacing="2">
                <tr>
                    <td valign="top">
                        <h3>{{__( 'Top 5 countries' )}}</h3>
                        <script>
                        {!! $charts['countries']['top5'] !!}
                        </script>
                        <div id='stat_pie_top5' class='stats_pie pie' style='display:block; width:300px; height:300px'>
                        </div>
                        <p><a href="#" class='details hide-if-no-js' id="more_countries">{{__( 'Click for more details' )}}</a></p>
                        <ul id="details_countries" style="display:none" class="no_bullet">

                            @foreach( $countries as $code => $details )
                                <li>
                                    <img width="32" alt="{{$details['country_name']}}'s flag" src='{{$details['flag']}}' />
                                    {{__($details['country_name'])}} : {{$details['hits']}}
                                    @if($details['hits'] > 1)
                                        {{__( 'hits')}}
                                    @else
                                        {{__( 'hit')}}
                                    @endif
                                </li>
                            @endforeach
                        </ul>

                    </td>
                    <td valign="top">
                        <h3>{{__( 'Overall traffic' )}}</h3>
                        {!! $charts['countries']['map'] !!}
                    </td>
                </tr>
            </table>
        @else
            <p>{{__( 'No country data.' )}}</p>
        @endif
    </div>


    <div id="stat_tab_sources" class="tab">
        <h2>{{__( 'Traffic sources' )}}</h2>

        @if ( $referrers )

        <table border="0" cellspacing="2">
            <tr>
                <td valign="top">
                    <h3>{{__( 'Direct vs Referrer Traffic' )}}</h3>
                    <script>
                        {!! $charts['referrers']['directVStraffic'] !!}
                    </script>
                    <div id='stat_pie_referrer' class='stats_pie pie' style='display:block; width:300px; height:300px'>
                    </div>
                </td>
                <td valign="top">
                    <p>
                        {{__( 'Direct traffic:' )}}
                        @if(($direct = $referrers['direct']) > 1)
                            {!! sprintf('<strong>%s</strong> hits', $direct ) !!}
                        @else
                            {!! sprintf('<strong>%s</strong> hit', $direct ) !!}
                        @endif
                    </p>
                    <p>
                        {{__( 'Referrer traffic:' )}}
                        @if(($notdirect = $referrers['notdirect']) > 1)
                            {!! sprintf('<strong>%s</strong> hits', $notdirect ) !!}
                        @else
                            {!! sprintf('<strong>%s</strong> hit', $notdirect ) !!}
                        @endif
                    </p>

                </td>
            </tr>
        </table>
        @else
            <p>{{__( 'No referrer data.' )}}</p>
        @endif
    </div>
    </div>
@endsection
