@extends('layouts.app')

@section('content')
    <h3 class="tlbx-variant-heading">Report</h3>

    <h5>{{ __('Thank you for helping us to keep this platform appropriate !') }}</h5>

    <p>
        The url <a href="{{{url('/' . session('alias')->alias )}}}">{{ session('alias')->url['url'] }}</a> with the alias <code>{{ session('alias')->alias }}</code> have been reported.
    </p>
@endsection
