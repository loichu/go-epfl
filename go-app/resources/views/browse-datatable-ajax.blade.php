@extends('layouts.app')

@section('content')
    <h3 class="tlbx-variant-heading">Browse links</h3>

    <!-- See https://epfl-idevelop.github.io/elements/#/molecules/tables ! -->
    <div class="table-responsive-md">
        <table id="aliases" class="ssdataTable table table-striped table-bordered table-hover" >
            <thead>
                <tr>
                    <th>Link</th>
                    <th>Alias</th>
                    <th>Clicks</th>

                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
@endsection

@section('javascript')
<script>
console.time('start');
$(function(){
    // [{"id":6932,"alias":"toto","url":"toto.com","trucatedurl":"toto.com","created_at":"2019-06-26 22:52:26","updated_at":"2019-06-26 22:52:26","hidden":false,"status_code":null,"sciper":"169419","username":"nborboen","clicks":0,"flags":0,"obsolete":null,"deleted_at":null}, ...]

    // $('#aliases').DataTable( {
    //     "processing": true,
    //     "serverSide": true,
    //     "ajax": {
    //         "url": '/api/v1/datatable', // '/api/v1/aliases',
    //         "type": "GET"
    //     },
    //     "columns": [
    //         { "data": "id" },
    //         { "data": "alias" },
    //         { "data": "clicks_count" }
    //     ]});
    $('#aliases').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": '/api/v1/datatableView2', // '/api/v1/aliases',
            "type": "GET"
        },
        "columns": [
            { "data": "url" },
            { "data": "alias" },
            { "data": "clicks" }
        ],
        "columnDefs": [ {  // https://stackoverflow.com/questions/29081383/how-to-format-customize-data-in-table-columns-using-datatables-server-processing#29081970
            "targets": 0,  // index of column starting from 0
            "data": "url", // this name should exist in your JSON response
            "render": function ( data, type, full, meta ) {
                return '<a href="https://go.epfl.ch/'+full.alias+'" title="'+full.url+'">'+full.trucatedurl+'</a>';
            }
        }]
    });
});

</script>
@endsection
