@extends('layouts.app')

@section('content')
    <h3 class="tlbx-variant-heading">GO EPFL - Statistics</h3>

    <p>
        Today, <a href="/">go.epfl.ch</a> inventories <b>{{$nb_urls}} shortened URLs</b> of which <b>{{$nb_epfl}} are
        internal to EPFL</b> (i.e. <a href="https://epfl.ch">epfl.ch</a>) – either <b>{{$percentage}}%</b>. Since it has
        been launched, this website has done <b>{{$nb_redirects}} redirects</b>!
    </p>
    <p>
        There are <b>{{$nb_aliases}} aliases</b>. The average number of aliases per URL is <b>{{$aliases_per_url}}
        (aliases/url)</b>. The most aliased URL is <b>{{$most_aliased_url}} with {{$most_aliases}} aliases</b>.
    </p>
@endsection
