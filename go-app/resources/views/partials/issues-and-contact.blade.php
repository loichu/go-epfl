<section class="mb-4">
    <!--Section heading-->
    <h2 class="h1-responsive font-weight-bold text-center my-4">Contact us</h2>
    <!--Section description-->
    <p class="text-center w-responsive mx-auto mb-5">
        It will be much appreciated if you can open an issue on <a href="https://gitlab.com/epfl-idevfsd/go-epfl/issues" target="_blank">https://gitlab.com/epfl-idevfsd/go-epfl/issues</a>.<br />
        You can also send an email to <a href="mailto:incoming+epfl-idevfsd-go-epfl-10607222-issue-@incoming.gitlab.com">incoming+epfl-idevfsd-go-epfl-10607222-issue-@incoming.gitlab.com</a> to automagically create a request.<br />
        Finally, you can contact the maintainers by email <a href="mailto:go@groupes.epfl.ch">here</a>.
    </p>

    <div class="row">

        <!--Grid column-->
        <div class="col-md-8 mb-md-0 mb-5 offset-md-2">
            <form id="contact-form" name="contact-form" action="{{ url('/emailconfirm') }}" method="POST">
                @csrf
                <div class="form-group">
                    <label for="select-recipient">Recipient</label>
                    <select id="select-recipient" name="recipient" class="custom-select">
                        <option value="bug">Declare a bug</option>
                        <option value="admin">Contact administrators</option>
                    </select>
                </div>

                <div class="form-group">
                    <label for="subject" class="">Subject</label>
                    <input type="text" id="subject" name="subject" class="form-control">
                </div>

                <div class="form-group">
                    <label for="message">Your message</label>
                    <textarea type="text" id="message" name="message" rows="2" class="form-control md-textarea"></textarea>
                </div>

                <div class="row justify-content-center mt-4">
                    <input type="submit" value="Send" name="submit" class="btn btn-primary" />
                </div>
            </form>
        </div>
    </div>
</section>
