<form method="post" action="{{$action ?? '/aliasconfirm'}}" name="frm_alias">
    @csrf

    <input type="hidden" name="original_alias" value="{{{$prev_alias ?? ''}}}"/>

    <div class="form-group mb-2">
        <input type="text"
               required="true"
               class="form-control form-control-lg"
               name="url"
               id="urlInput"
               placeholder="Existing url, e.g. https://www.epfl.ch/campus/services/communication/audiences-canaux/epfl-magazine/"
               value="{{$prev_url ?? ''}}"
        />
    </div>

    <div class="input-group input-group-lg">
        <div class="input-group-prepend">
            <span class="input-group-text" id="vanityURL">https://go.epfl.ch/</span>
        </div>
        <input type="text"
               required="true"
               class="form-control form-control-lg"
               name="alias"
               id="aliasInput"
               aria-describedby="vanityURL"
               placeholder="Alias to use, e.g. mag"
               value="{{$prev_alias ?? ''}}"
        />
        <span class="d-flex flex-wrap align-items-center">
            <a href="#" onclick="randomizeAlias()" >
                <img src="/svg/reload.svg" width="30px" />
            </a>
        </span>
    </div>

    <fieldset class="form-group">

        <div class="row pt-2">
            <legend class="col-form-label col-md-4 col-sm-3 pt-0">Hidden (<a href="#info">?</a>)</legend>
            <div class="col-md-8 col-sm-9">
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="checkbox" name="hidden" id="hidden" value="hidden" {{$hidden ? 'checked=true' : ''}}/>
                    <label class="form-check-label" for="hidden">
                        &nbsp;Make this alias hidden
                    </label>
                </div>
            </div>
        </div>

        <div class="row">
            <legend class="col-form-label col-md-4 col-sm-3 pt-0">TTL (<a href="#info">?</a>)</legend>
            <div class="col-md-8 col-sm-9">
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="ttl" id="ttl1" value="6"
                        {{isset($ttl) && $ttl == 6 ? 'checked=true' : ''}}/>
                    <label class="form-check-label" for="ttl1">
                        6 months
                    </label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="ttl" id="ttl2" value="12"
                        {{isset($ttl) && $ttl == 12 ? 'checked=true' : ''}}/>
                    <label class="form-check-label" for="ttl2">
                        1 year
                    </label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="ttl" id="ttl3" value="24"
                        {{isset($ttl) && $ttl == 24 ? 'checked=true' : ''}}/>
                    <label class="form-check-label" for="ttl3">
                        2 years
                    </label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="ttl" id="ttl4" value="0"
                        {{isset($ttl) && $ttl == 0 ? 'checked=true' : ''}}/>
                    <label class="form-check-label" for="ttl4">
                        &infin;
                    </label>
                </div>
                @isset($obsolescence_date)
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="ttl" id="ttl5" value="-" checked="true"/>
                    <label class="form-check-label" for="ttl5">
                        will expire at: {{$obsolescence_date}}
                    </label>
                </div>
                @endisset
            </div>
        </div>

        <button type="submit" class="btn btn-primary btn-lg btn-block">{{$submitButton}}</button>

    </fieldset>

</form>
<script>
// https://stackoverflow.com/a/26410127
// str byteToHex(uint8 byte)
//   converts a single byte to a hex string
function byteToHex(byte) {
    return ('0' + byte.toString(16)).slice(-2);
}
// str generateId(int len);
//   len - must be an even number (default: 40)
function generateId(len = 40) {
    var arr = new Uint8Array(len / 2);
    window.crypto.getRandomValues(arr);
    return Array.from(arr, byteToHex).join("");
}
function randomizeAlias() {
    if (confirm('Are you sure you want to generate a random hash as alias?')) {
        $('#aliasInput').val(generateId(6))
    }
}
</script>
