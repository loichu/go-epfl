@extends('layouts.app')

@section('content')
  <h3 class="tlbx-variant-heading">General {{$page}}</h3>
  
  <button class="collapse-title collapse-title-desktop collapsed" type="button" data-toggle="collapse" data-target="#What-is-URL-shortening" aria-expanded="false" aria-controls="What-is-URL-shortening">
    What is URL shortening ?
  </button>
  <div class="collapse collapse-item collapse-item-desktop" id="What-is-URL-shortening">
    <p>
      URL shortening is a technique in which a URL may be made substantially 
      shorter in length and have more meaning.<br />
      Example:
      <a href="https://go.epfl.ch/magazine">https://www.epfl.ch/campus/services/communication/en/audiences-and-channels/epfl-magazine/</a> 
      becomes 
      <a href="https://go.epfl.ch/magazine">https://go.epfl.ch/magazine</a>.
    </p>
  </div>


  <button class="collapse-title collapse-title-desktop collapsed" type="button" data-toggle="collapse" data-target="#What-is-the-purpose-of-this-site" aria-expanded="false" aria-controls="What-is-the-purpose-of-this-site">
    What is the purpose of this site ?
  </button>
  <div class="collapse collapse-item collapse-item-desktop" id="What-is-the-purpose-of-this-site">
    <p>
      Besides from the url shortening this site has other goals:<br />
      <ul>
        <li>A way to remember all those URLs so complicated</li>
        <li>To have a directory with EPFL's internal links</li>
        <li>To have a quick and safe (and and corporate compliant) way to shorten URLs</li>
      </ul>
      If you read french, have a look to this <a href="https://go.epfl.ch/gofi">article</a> in the Flash Informatique.
    </p>
  </div>


  <button class="collapse-title collapse-title-desktop collapsed" type="button" data-toggle="collapse" data-target="#Is-go-epfl-ch-an-official-EPFL-service" aria-expanded="false" aria-controls="Is-go-epfl-ch-an-official-EPFL-service">
    Is go.epfl.ch an official EPFL service ?
  </button>
  <div class="collapse collapse-item collapse-item-desktop" id="Is-go-epfl-ch-an-official-EPFL-service">
    <p>
      <a href="://go.epfl.ch">go.epfl.ch</a> is born due to necessity of a mailing (read this <a href="https://go.epfl.ch/gofi">article</a> about it) and it is not yet an EPFL service.
      Communication may be addressed to <a href="mailto:go@groupes.epfl.ch">go@groupes.epfl.ch</a>.
    </p>
  </div>


  <button class="collapse-title collapse-title-desktop collapsed" type="button" data-toggle="collapse" data-target="#What-system-is-used" aria-expanded="false" aria-controls="What-system-is-used">
    What system is used ?
  </button>
  <div class="collapse collapse-item collapse-item-desktop" id="What-system-is-used">
    <p>
      The first version of <a href="://go.epfl.ch">go.epfl.ch</a> was a mix between PHURL (https://code.google.com/archive/p/phurl/) with some parts of YOURLS (http://yourls.org/).
    </p>
    <p>
      Early 2019, Loïc Humbert (an IT apprentice) was asked to refresh the whole system during his end of apprentiship work (called TPI - Travail Pratique Individuel). 
      While still running some parts of YOURLS, the website has been ported to the PHP Framework <a href="https://laravel.com/">Laravel</a>. 
      You may be interessed to have a look to the <a href="https://gitlab.com/epfl-idevdsd/go-epfl">source code</a>.
    </p>
  </div>


  <button class="collapse-title collapse-title-desktop collapsed" type="button" data-toggle="collapse" data-target="#Are-go-cut-and-short-the-same" aria-expanded="false" aria-controls="Are-go-cut-and-short-the-same">
    Are go.epfl.ch, cut.epfl.ch and short.epfl.ch the same ?
  </button>
  <div class="collapse collapse-item collapse-item-desktop" id="Are-go-cut-and-short-the-same">
    <p>
      Yes they are sharing the same database, but go.epfl.ch is a 1 char shorter than cut.epfl.ch and 2 chars shorter than short.epfl.ch...
    </p>
  </div>


  <button class="collapse-title collapse-title-desktop collapsed" type="button" data-toggle="collapse" data-target="#Feature-request-contributing-issues" aria-expanded="false" aria-controls="Feature-request-contributing-issues">
    Feature request, contributing, issues
  </button>
  <div class="collapse collapse-item collapse-item-desktop" id="Feature-request-contributing-issues">
    @include('partials.issues-and-contact') 
  </div>


  <h3 class="tlbx-variant-heading">Usage {{$page}}</h3>

  <button class="collapse-title collapse-title-desktop collapsed" type="button" data-toggle="collapse" data-target="#Can-an-URL-have-different-alias" aria-expanded="false" aria-controls="Can-an-URL-have-different-alias">
    Can an URL have different alias ?
  </button>
  <div class="collapse collapse-item collapse-item-desktop" id="Can-an-URL-have-different-alias">
    <p>
      Yes. A same URL can have from 1 to N alias.
    </p>
  </div>

  <button class="collapse-title collapse-title-desktop collapsed" type="button" data-toggle="collapse" data-target="#What-is-a-hidden-alias" aria-expanded="false" aria-controls="Can-an-URL-have-different-alias">
    What is a hidden alias ?
  </button>
  <div class="collapse collapse-item collapse-item-desktop" id="What-is-a-hidden-alias">
    <p>
      TODO
    </p>
  </div>


  <button class="collapse-title collapse-title-desktop collapsed" type="button" data-toggle="collapse" data-target="#I-have-question-or-problem" aria-expanded="false" aria-controls="I-have-question-or-problem">
    I have question or problem related to go.epfl.ch
  </button>
  <div class="collapse collapse-item collapse-item-desktop" id="I-have-question-or-problem">
    @include('partials.issues-and-contact') 
  </div>
@endsection
