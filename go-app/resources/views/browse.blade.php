@extends('layouts.app')

@section('content')
    <h3 class="tlbx-variant-heading">Browse links</h3>

    <!-- See https://epfl-idevelop.github.io/elements/#/molecules/tables ! -->

    <table id="aliases" class="table table-boxed table-sortable" >
        <thead>
            <tr>
                <th>Link</th>
                <th>Alias</th>
                <th>Clicks</th>
                <th>Info</th>
                @auth
                    <th>Report</th>
                @endauth
            </tr>
        </thead>
        <tbody>
        @foreach($aliases as $alias)
            <tr>
                <td><a href="{{env('app.url')}}{{{$alias->alias}}}">{{{$alias->url->url}}}</a></td>
                <td><a href="{{env('app.url')}}{{{$alias->alias}}}">{{{$alias->alias}}}</a></td>
                <td>{{$alias->clicks_count}}</td>
                <td><a href="/{{env('app.url')}}info/{{{$alias->alias}}}">📈</a></td>
                @auth
                    <td><a href="/{{env('app.url')}}report/{{{$alias->alias}}}">report</a></td>
                @endauth
            </tr>
        @endforeach
        </tbody>
    </table>

    <nav aria-label="Page navigation">
        <ul class="pagination justify-content-center">
        @if(!empty($displayed_pages['before']))
            <li class="page-item">
                <a class="page-link" href="{{$aliases->url(1)}}" aria-label="Go to first page">
                    <svg class="icon" aria-hidden="true"><use xlink:href="#icon-chevron-last-left"></use></svg>
                </a>
            </li>
            <li class="page-item">
                <a class="page-link" href="{{$aliases->previousPageUrl()}}" aria-label="Go to previous page">
                    <svg class="icon" aria-hidden="true"><use xlink:href="#icon-chevron-left"></use></svg>
                    <span class="sr-only">Previous</span>
              </a>
            </li>
            @foreach($displayed_pages['before'] as $number => $url)
                <li class="page-item">
                    <a class="page-link" href="{{$url}}" aria-current="page">
                        {{$number}}
                    </a>
                </li>
                @endforeach
        @endif
        <li class="page-item active">
            <a class="page-link" href="#" aria-current="page">
                {{$aliases->currentPage()}}
                <span class="sr-only">(Current page)</span>
            </a>
        </li>
        @if(!empty($displayed_pages['after']))
            @foreach($displayed_pages['after'] as $number => $url)
                <li class="page-item">
                    <a class="page-link" href="{{$url}}" aria-current="page">
                        {{$number}}
                    </a>
                </li>
            @endforeach
            <li class="page-item">
                <a class="page-link" href="{{$aliases->nextPageUrl()}}" aria-label="Go to next page">
                    <svg class="icon" aria-hidden="true"><use xlink:href="#icon-chevron-right"></use></svg>
                    <span class="sr-only">Previous</span>
                </a>
            </li>
            <li class="page-item">
                <a class="page-link" href="{{$aliases->url($aliases->lastPage())}}" aria-label="Go to last page">
                    <svg class="icon" aria-hidden="true"><use xlink:href="#icon-chevron-last-right"></use></svg>
                </a>
            </li>
        @endif
        </ul>
    </nav>
@endsection
