@extends('layouts.app')

@section('content')

    <h3 class="tlbx-variant-heading">GO EPFL - URL shortener</h3>

    <h5>Reveal URL</h5>
    @if (session('message'))
        <div class="alert alert-danger">
            {{ session('message') }}
        </div>
    @endif

    <p>
        Accessing "<code>{{ $alias }}</code>" via the link <a href="{{{ url($alias) }}}">{{{ url($alias) }}}</a>
        will redirect you to the website <br />
        <a href="{{{ url($alias) }}}">{{{ $url }}}</a>.
    </p>
    <br />
    <h5>Report</h5>
    <p>
        If needed, you can report this Alias/URL by following this link:
        <a href="{{{ url('/report/'.$alias) }}}">{{{ url('/report/'.$alias) }}}</a>.
    </p>

@endsection
