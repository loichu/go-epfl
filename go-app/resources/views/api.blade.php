@extends('layouts.app')

@section('content')
    <h3 class="tlbx-variant-heading">GO EPFL - API</h3>

    <h5>Prerequisites</h5>
    <p>
        <a href="https://go.epfl.ch">Go.epfl.ch</a> comes with an <a href="">API</a>. In order to use it, there are 2  prerequisites:
        <ol>
            <li>Be a member of the EPFL community (e.g. have a <a href="">Gaspar</a> login)</li>
            <li>Have an API token (you can generate one in your <a href="">profile</a>)</li>
        </ol>
    </p>
    <br />
    <h5>Infos</h5>
    <p>
        [WIP]
    </p>
    <p>
        <a name="aliases"><b>aliases</b></a>: Get all aliases (paginated)<br/>
        <code>GET http://go.epfl.ch/api/v1/aliases</code>
    </p>
    <p>

        <a name="alias/{alias}"><b>alias/{alias}</b></a>: Get a specific alias<br/>
        <code>GET http://go.epfl.ch/api/v1/alias/{alias}</code>
    </p>
    <p>
        <a name="alias"><b>alias</b></a>: Create an alias<br/>
        <code>POST http://go.epfl.ch/api/v1/alias</code><br/>
        <ul>Params:
                    <li><code>token: 0123456789abcdef</code></li>
                    <li><code>alias: zombo</code></li>
                    <li><code>url: https://zombo.com</code></li>
                </ul>
    </p>
    <h5>CURL example</h5>
    <p>
    <code>curl -X POST \<br>
        &nbsp;&nbsp;&nbsp;&nbsp;--header 'Content-Type: application/json' \<br>
        &nbsp;&nbsp;&nbsp;&nbsp;--header 'Accept: application/json' \<br>
        &nbsp;&nbsp;&nbsp;&nbsp;-d '{"token":"0123456789abcdef", "alias":"zombo", "url":"https://zombo.com"}' \<br>
        'http://go.epfl.ch/api/v1/alias'
    </code>
    </p>
@endsection
