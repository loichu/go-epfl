@extends('layouts.app')

@section('content')
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
        google.load('visualization', '1.0', {'packages':['corechart', 'geochart']});
    </script>

    <h3 class="tlbx-variant-heading">Info</h3>
    <div class="row">
        <div class="col-md-8">
            <div id="total_hits">
                {{__('Hits count')}}: {{$alias->clicks_count}}
            </div>
            <div id="sorry-nostats">
                <b>{{__('Sorry but you have no hits yet.')}}</b>
            </div>
        </div>
        <div class="col-md-4">
            <div class="box" id="qrcode-container">
                <h3>QR Code</h3>
                <img id="qrcode" src="data:img/png;base64, {!! $qrcode !!}" alt="qrcode for {{$alias->url->url}}"/>
            </div>
        </div>
    </div>






@endsection
