@extends('layouts.app')

@section('content')

    <h5>Update alias</h5>

    @include('partials.edit_alias', [
        'prev_url'          => $url,
        'prev_alias'        => $alias,
        'hidden'            => $hidden,
        'obsolescence_date' => $obsolescence_date,
        'action'            => '/update/alias',
        'submitButton'      => 'Update'
    ])
@endsection
