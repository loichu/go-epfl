{{-- 
    https://github.com/epfl-idevelop/epfl-news/blob/master/src/templates/menu/nav-main-web2018.html 
    Yeah, I know, you too wish to have a single menu... duh ! 
--}}
<div class="overlay"></div>
<nav class="nav-main" id="main-navigation">
  <div class="nav-wrapper">
    <div class="nav-container current-menu-parent">
      <ul class="nav-menu">
          @include('layouts.menu.partials.list')
      </ul>
    </div>
  </div>
</nav>