<?php # https://epfl-idevelop.github.io/elements/#/organisms/footer/light-dark-en/full ?>
<footer class="footer-light fixed-bottom d-none d-md-block" role="contentinfo">
  <div class="tlbx-component-footer-light-dark-en bg-dark">
    <div>&nbsp;
      <!-- poor man's space on Friday's night -->
    </div>
    <div class="row">
      <div class="col-xl-11 mx-xl-auto">
        <div class="row">
          <div class="col-6 mx-auto mx-md-0 mb-4 col-md-3 col-lg-2">
            <a href="#">
              <img src="/svg/epfl-logo-negative.svg" alt="Logo EPFL, École polytechnique fédérale de Lausanne" class="img-fluid">
            </a>
          </div>
          <div class="col-md-9 col-lg-10">
            <div class="ml-md-2 ml-lg-5">
              <ul class="list-inline list-unstyled">
                <li class="list-inline-item text-white">Contact</li>
                <li class="list-inline-item text-gray-300 pl-3"><small>EPFL CH-1015 Lausanne</small></li>
                <li class="list-inline-item text-gray-300 pl-3"><small>+41 21 693 11 11</small></li>
              </ul>
              <p class="footer-light-socials">
                <small>Follow the EPFL on social media</small>
                <span>
                  <a href="https://www.facebook.com/epflcampus" class="social-icon social-icon-facebook social-icon-negative">
                    <svg class="icon" aria-hidden="true">
                      <use xlink:href="#icon-facebook"></use>
                    </svg>
                    <span class="sr-only">Follow us on Facebook.</span>
                  </a>
                  <a href="https://twitter.com/epfl" class="social-icon social-icon-twitter social-icon-negative">
                    <svg class="icon" aria-hidden="true">
                      <use xlink:href="#icon-twitter"></use>
                    </svg>
                    <span class="sr-only">Follow us on Twitter.</span>
                  </a>
                  <a href="http://instagram.com/epflcampus" class="social-icon social-icon-instagram social-icon-negative">
                    <svg class="icon" aria-hidden="true">
                      <use xlink:href="#icon-instagram"></use>
                    </svg>
                    <span class="sr-only">Follow us on Instagram.</span>
                  </a>
                  <a href="https://www.youtube.com/user/epflnews" class="social-icon social-icon-youtube social-icon-negative">
                    <svg class="icon" aria-hidden="true">
                      <use xlink:href="#icon-youtube"></use>
                    </svg>
                    <span class="sr-only">Follow us on Youtube.</span>
                  </a>
                  <a href="https://www.linkedin.com/school/epfl/" class="social-icon social-icon-linkedin social-icon-negative">
                    <svg class="icon" aria-hidden="true">
                      <use xlink:href="#icon-linkedin"></use>
                    </svg>
                    <span class="sr-only">Follow us on LinkedIn.</span>
                  </a>
                </span>
              </p>

              <div class="footer-legal">
                <div class="footer-legal-links">
                  <a href="https://epfl-idevelop.github.io/elements/#/doc/accessibility.html">{{ __('Accessibility') }}</a>
                  <a href="https://www.epfl.ch/about/overview/overview/regulations-and-guidelines/">{{ __('Legals') }}</a>
                  <a href="{{ route('contact') }}">{{ __('Contact') }}</a>
                  <a href="https://gitlab.com/epfl-idevfsd/go-epfl">{{ __('Source code') }}</a>&nbsp;<p id="app_version">{{$app_version}}</p>
                </div>
                <div>
                  <p>&copy; 2018-<?php echo date('Y') ?> {{ __('EPFL, all rights reserved') }}</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</footer>
