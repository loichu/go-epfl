{{-- From EPFL https://epfl-idevelop.github.io/elements/#/organisms/header/light-fr/full --}}
<header role="banner" class="header header-light navbar-laravel" id="app-layout">
  <div class="drawer mr-3 mr-xl-0">
    <button class="drawer-toggle">
      <svg class="icon" aria-hidden="true"><use xlink:href="#icon-chevron-right"></use></svg>
    </button>
    <a href="//www.epfl.ch" class="drawer-link">
      <span class="text">
        Go to EPFL main site
      </span>
    </a>
  </div>

  <div class="header-light-content">
    <a class="logo" href="/">
      <img src="/svg/epfl-logo.svg" alt="Logo EPFL, École polytechnique fédérale de Lausanne" class="img-fluid">
    </a>
    <h1><acronyme title="EPFL URL Shortener">go.epfl.ch</acronyme></h1>

    @include('layouts.menu.menu')

    @include('layouts.menu.mobile.search')
    @include('layouts.menu.partials.search')

    <nav class="nav-lang nav-lang-short ml-auto">
      <!-- Right Side Of Navbar -->
      <ul class="navbar-nav ml-auto">
        <!-- Authentication Links -->
        @guest
          <li class="nav-item">
            <span><a href="{{ route('login') }}">{{ __('Login') }}</a></span>
          </li>
        @else
          <li class="nav-item">
            <span><a href="/admin/">{{ __('Admin') }}</a></span>
          </li>
          <li class="nav-item">
            <span><a href="/profile">{{ Auth::user()->username }} ⌅</a></span>
          </li>
          <li class="nav-item">
            <span><a href="/logout">{{ __('Logout') }} →</a></span>
          </li>
        @endguest
      </ul>
      <ul>
        <li>
          <a href="#" aria-label="Français" alt="">FR</a>
        </li>
        <li>
          <span class="active" aria-label="English">EN</span>
        </li>
      </ul>
    </nav>

    @include('layouts.menu.mobile.burger')

  </div>

</header>
