@extends('layouts.skeleton')
@section('body')
    <div class="main-container">
        @include('layouts.menu.mobile.menu')
        <div id="app-layout" class="container">
            <main class="py-4">
                @include('layouts.errors')
                @yield('content')
            </main>
        </div>
    </div>
@endsection
