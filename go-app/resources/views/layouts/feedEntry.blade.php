  <entry>
    <title type="html">{{ env('APP_URL') }}{{ $entry->alias }} → {{{$entry->url->url}}}</title>
    <link href="{{ env('APP_URL') }}{{ $entry->alias }}"/>
    <id>tag:{{ env('APP_URL') }}{{ $entry->alias }}:@php echo sha1($entry->alias); @endphp</id>
    <updated>{{ $entry->updated_at }}</updated>
    <created>{{ $entry->created_at }}</created>
    <summary type="html">
      <![CDATA[
      The link <a href="{{ env('APP_URL') }}{{ $entry->alias }}">{{{$entry->url->url}}}</a> 
      has been shortened on {{ $entry->created_at }}.
      <br />
      Its alias is <b>{{ $entry->alias }}</b>.
      <br />
      It has been clicked {{ $entry->clicks_count }} times.
      <br />
      Please visit it here: <a href="{{ env('APP_URL') }}{{ $entry->alias }}">{{{$entry->url->url}}}</a>.
      <br /><hr />
      <a href="{{ env('APP_URL') }}">go.epfl.ch</a> — EPFL URL shortener (<a href="{{ env('APP_URL') }}/contact">contact</a>)
      ]]>
    </summary>
  </entry>