@extends('layouts.app')

@section('content')
    <h3 class="tlbx-variant-heading">GO EPFL - URL shortener</h3>
    <div class="aliasCreationConfirmation">
        <div class="alert alert-success" role="alert">
            <h4 class="alert-heading">Well done!</h4>
            <p>
                The URL <a href="{{{ $new_url }}}" target="_blank">{{{ $alias['url']->url }}}</a> has been shortened to <a href="{{{ $new_url }}}">go.epfl.ch/{{{ $alias['alias'] }}}</a>.
            </p>
            <hr>
            <p class="mb-0">Whenever you need to, you can edit this alias (or URL) from your profile anytime. The direct access to the link of the edit page is <a href="//go.epfl.ch/edit/alias/{{{ $alias['alias'] }}}">go.epfl.ch/edit/alias/{{{ $alias['alias'] }}}</a>.</p>
        </div>
        @if ($alias['hidden'] == 'true')
        <div class="alert alert-warning" role="alert">
            <h4 class="alert-heading">Private ?</h4>
            <p>
                The URL <a href="{{{ $new_url }}}" target="_blank">{{{ $alias['url']->url }}}</a> has been shortened using the "private" option. That means it's not listed in the <a href="/browse">browse</a> page or in the <a href="/feed">RSS</a> feed, but it's still have the same features than another alias.
            </p>
            <hr>
            <p class="mb-0">More details about private aliases can be found in the <a href="/faq/#What-is-a-private-alias">FAQ</a>.</p>
        </div>
        @endif
        <div class="alert alert-info" role="alert">
            <h4 class="alert-heading">Do you know ?</h4>
            <p>
                You can access some basic usage information on this alias in this page: <a href="//go.epfl.ch/info/{{{ $alias['alias'] }}}" target="_blank">/info/{{{ $alias['alias'] }}}</a>.
            </p>
        </div>
    </div>
@endsection
