@extends('layouts.app')

@section('content')
    <h3 class="tlbx-variant-heading">{{ $user->username }}'s profile</h3>

    <h5>Your aliases</h5>
    <div id="aliasesList">
        <ul>
            @foreach($aliases as $alias)
                <li>
                    {{$alias->alias}} &rarr; {{$alias->url->url}}
                    <a href="/edit/alias/{{{$alias->alias}}}">edit</a>
                </li>
            @endforeach
        </ul>
    </div>
    <br />
    <h5>API Token</h5>
    <div class="card ">
        <div class="row p-4">
            @if($user->api_token)
                <div class="col-md-6">
                    <b>Token:</b> {{$user->api_token}}
                </div>
                <div class="col-md-2">
                    <a href="/token/delete" class="btn btn-danger">Delete</a>
                </div>
                <div class="col-md-2">
                    <a href="/token/generate" class="btn btn-danger">Regenerate</a>
                </div>
            @else
                <div class="col-md-4 offset-4">
                    <a href="/token/generate" class="btn btn-danger">Generate token</a>
                </div>
            @endif
        </div>
    </div>
    <h5>Preferences</h5>
    <i>This is a work in progress...</i>
    <br />
    <div class="row">
        <div class="col-md-6">
            <p>Hash generation length</p>
        </div>
        <div class="col-md-4">
            <p>(default 8)</p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <p>Show alert on hash generation</p>
        </div>
        <div class="col-md-4">
            <p>(default on)</p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <p>Default TTL</p>
        </div>
        <div class="col-md-4">
            <p>(default 6 month)</p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <p>Alias hidden</p>
        </div>
        <div class="col-md-6">
            <p>(default off)</p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <p>Result per page on browse</p>
        </div>
        <div class="col-md-6">
            <p>(default 10)</p>
        </div>
    </div>
@endsection
