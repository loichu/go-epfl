@extends('layouts.app')

@section('content')
    <h3 class="tlbx-variant-heading">Browse links</h3>

    <!-- See https://epfl-idevelop.github.io/elements/#/molecules/tables ! -->
    <div class="table-responsive-md">
        <table id="aliases" class=" NodataTable table table-striped table-bordered table-hover" >
            <thead>
                <tr>
                    <th>Link</th>
                    <th>Alias</th>
                    <th>Clicks</th>
                    <th class="no-sort">Info</th>
                    @auth
                    <th class="no-sort">Report</th>
                    @endauth
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>

    </div>
@endsection

@section('javascript')
<script>
console.time('start');
$(function(){
    console.timeLog('start');
    console.time('ready');
    console.timeLog('ready');

    var aliases = @json($aliases); // [{"id":6932,"alias":"toto","url":"toto.com","trucatedurl":"toto.com","created_at":"2019-06-26 22:52:26","updated_at":"2019-06-26 22:52:26","hidden":false,"status_code":null,"sciper":"169419","username":"nborboen","clicks":0,"flags":0,"obsolete":null,"deleted_at":null}, ...]
    console.time('data');
    var data = aliases.map(function(i) {
        console.debug(i);
        return ['<a href="/'+i.alias+'">'+i.trucatedurl+'</a>', '<a href="/'+i.alias+'">'+i.alias+'</a>', i.clicks, '<a href="/info/'+i.alias+'">📈</a>', i.flags]
    });
    console.timeLog('data')

    console.time('render')

    console.log("Loading " + aliases.length + " rows...");
    $('#aliases').dataTable({
        data: data,
        deferRender: true,
    });

    console.timeLog('render')
});

</script>
@endsection
