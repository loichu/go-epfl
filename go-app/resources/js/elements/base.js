/* globals jQuery, Tablesaw */
const objectFitImages = require('object-fit-images');

const jQuery = require('jquery')
const upload = require('./upload').default;
const breadcrumb = require('./breadcrumb').default;
const share = require('./share').default;
const selectMultiple = require('./select-multiple').default;
const svgIcons = require('./svg-icons').default;
const nav = require('./nav-main.js').default;
const drawer = require('./drawer.js').default;
const search = require('./search.js').default;
const coursebook = require('./coursebook.js').default;
const anchors = require('./anchors').default;

svgIcons(); // Must run as soon as possible

const init = () => {
  upload();
  selectMultiple();
  search();
  share();
  coursebook();
  //Tablesaw.init();
  nav();
  drawer();
  breadcrumb();
  anchors();

  // Init polyfill for Object Fit on IE11
  const isIE11 = !!window.MSInputMethodContext && !!document.documentMode;
  if (isIE11) {
    objectFitImages();
    jQuery('body').addClass('ie');
  }
};

if (undefined === window.sources) {
  (($) => {
    $(document).ready(() => init());
  })(jQuery);
}
