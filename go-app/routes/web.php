<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/phpinfo', function () {
    phpinfo();
});

Route::get('/', function () {
    return view('home', ['page' => 'home']);
})->name('home');

Route::get('/try', function () {
    dd(app()->routes->getRoutes());
});

// Confirm pages
Route::post('/aliasconfirm', 'AliasController@create')->name('aliasconfirm');
Route::post('/emailconfirm', 'PageController@contactSend')->name('emailconfirm');
Route::view('/reportconfirm', 'reportconfirm')->name('reportconfirm');

// Main pages
Route::get('/about', 'PageController@about')->name('about');
Route::get('/browse', 'BrowseController@index')->name('browse');
Route::get('/browsedt', 'BrowseController@index_datatable_cache')->name('browsedt');
Route::get('/browsedtajax', 'BrowseController@index_datatable_ajax')->name('browsedtajax');
Route::get('/browse/s/', 'BrowseController@search')->name('search');
Route::get('/stats', 'StatsController@index')->name('stats');
Route::get('/faq', 'PageController@FAQ')->name('FAQ');
Route::get('/contact', 'PageController@contact')->name('contact');
Route::get('/profile', 'UserController@details')->name('profile');
Route::get('/api', 'PageController@api')->name('api');
Route::get('/api/v1', 'PageController@api')->name('api');
Route::get('/reveal', 'PageController@revealInfo')->name('revealInfo');

// Specific pages
Route::get('/info/{alias}', 'AliasDetails@info')->name('info');
Route::get('/edit/alias/{alias}', 'AliasController@edit')->name('edit/alias');
Route::post('/update/alias', 'AliasController@update')->name('update/alias');
Route::get('/reveal/{alias}', function (App\Alias $alias){
    return view('reveal', [
        'alias' => $alias->alias,
        'url'   => $alias->url->url
    ]);
});
Route::get('/report/{alias}', 'FlagsController@report')->middleware('auth');
Route::get('/token/generate', 'UserController@generateToken');
Route::get('/token/delete', 'UserController@deleteToken');

// Syndication Feed
Route::get('/feed', 'FeedController@index')->name('feed');
Route::permanentRedirect('/rss', '/feed');
Route::permanentRedirect('/atom', '/feed');

// Admin / backoffice
Route::group([
    'prefix' => 'admin',
    'middleware' => ['admin']
], function () {
    Route::get('/', 'AdminController@index')->name('admin');

    Route::get('aliases', 'AdminController@aliases')->name('admin-aliases');
    Route::get('alias/{alias}', 'AdminController@editAlias')->name('admin-edit-alias');
    Route::delete('alias/delete/{alias}', 'AdminController@deleteAlias')->name('admin-delete-alias');
    Route::post('update/owner', 'AdminController@updateOwner')->name('admin-update-owner');

    Route::get('flags', 'AdminController@flags')->name('admin-flags');
    Route::get('people', 'AdminController@people')->name('admin-people');

    Route::get('blacklist', 'AdminController@blacklist')->name('admin-blacklist');
    Route::post('blacklist/append', 'AdminController@appendToBlacklist')->name('admin-blacklist-append');
    Route::delete('blacklist/delete/{keyword}', 'AdminController@deleteFromBlacklist')->name('admin-blacklist-delete');

    Route::get('advanced', 'AdminController@advanced')->name('admin-advanced');
    Route::post('add/administrator', 'AdminController@addAdministrator')->name('admin-add-administrator');
    Route::get('jobs/reliability_test', 'AdminController@runReliabilityTest')->name('admin-reliability-test');
});


// Login routes
Route::get('/login', 'Auth\LoginController@redirectToProvider')->name('login');
Route::get('/login/callback', 'Auth\LoginController@handleProviderCallback');
Route::get('/logout', 'Auth\LoginController@logout');

// Dynamic routes KEEP AT THE END
Route::get('/{alias}', 'RedirectController');
