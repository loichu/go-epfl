<?php

use App\Http\Resources\AliasResource;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::namespace('API')->group(function () {
    Route::prefix('v1')->group(function () {
        Route::get('aliases', function () {
            return AliasResource::collection(\App\Alias::public()->withCount('clicks')->paginate());
        });
        // https://scqq.blogspot.com/2018/10/laravel-57-tutorial-datatables-api-jquery.html
        Route::get('datatable', function () {
            if (Cache::has('aliases')) {
                $aliases = Cache::get('aliases');
            } else {
                Cache::put('aliases', \App\Alias::public()->withCount('clicks'), 30); //30 minutes
                $aliases = Cache::get('aliases');
            }
            // return Datatables::of(\App\Alias::public()->withCount('clicks'))->make(true);
            return Datatables::of($aliases)->make(true);
        });
        Route::get('datatableView', function () {
            return Datatables::of(DB::table('v_aliases')->get())->make(true);
        });
        Route::get('datatableView2', function () {
            if (Cache::has('aliasesV')) {
                $aliasesV = Cache::get('aliasesV');
            } else {
                Cache::put('aliasesV', DB::table('v_aliases')->get(), 30); //30 minutes
                $aliasesV = Cache::get('aliasesV');
            }
            return Datatables::of($aliasesV)->make(true);
        });
        Route::get('alias/{alias}', function (\App\Alias $alias) {
            return new AliasResource($alias);
        });
        Route::post('alias', 'AliasController@create');
    });
});
