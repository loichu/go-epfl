# GO EPFL

URL shortener for [EPFL](https://www.epfl.ch) community.


## Get started

### Requirements
* All scripts are made to work with any modern Linux system (systemd)
* [docker](https://docs.docker.com/install/linux/docker-ce/ubuntu/)
* [docker-compose](https://docs.docker.com/compose/install/)
* [nginx](http://nginx.org/en/docs/install.html)
* [git](https://git-scm.com/)

### Task runner (Just)

The task runner used to simplify maintenance is
[just](https://github.com/casey/just). It is written in `Rust` so you can either
[install it via Cargo](https://github.com/casey/just#cargo) or just [download
the binaries](https://github.com/casey/just#pre-built-binaries).

The recommended way is to use `Cargo` as it is the official compiler and package
manager for `Rust` and it is really easy to install. Everything is very
straight-forward.

### Deploy GO
Copy `.env.example` in `.env`:
```
cp .env.example .env
```

Clone the repository:
```
$ git clone https://gitlab.com/epfl-idevfsd/go-epfl.git
```
or via ssh:
```
$ git clone git@gitlab.com:epfl-idevfsd/go-epfl.git
```

Run the script that will setup everything:
```
$ just install
```

Then go to http://go.epfl.ch

To connect to Tequila authentication, you'll need a `CLIENT_ID` and
`CLIENT_SECRET`. If you have access to the team's keybase repository, you can
find them here, otherwise you won't be able to authenticate unless you change
the code for another authentication system.

### Develop GO
If you need a cleaned Docker environment, or want to restart from scratch, use
the command `$ just install`.
