from postgres_conn import PostgresDB
from mysql_conn import MySqlDB
import time
import extract
import insert

config_old_db = {
  'user': 'user',
  'password': 'password',
  'port': '3306',
  'host': '127.0.0.1',
  'database': 'go',
  'raise_on_warnings': True
}
old_db = MySqlDB(config_old_db)

config_new_db = {
  'dbname': 'goepfl_import', 
  'user': 'postgres', 
  'password': 'secret', 
  'host': 'localhost'
}
new_db = PostgresDB(config_new_db)

#
# ALIASES
#
print('Preparing aliases...')
start = time.time()
aliases = extract.urls(old_db)
for alias in aliases:
    alias['url_id'] = extract.url_id(new_db, alias['url'])
    if alias['url_id'] == None:
        alias['url_id'] = insert.url(new_db, alias)
end = time.time()
elapsed = end - start
print('Aliases ready in ' + str(elapsed) + ' seconds')

print('Inserting aliases...')
start = time.time()
for alias in aliases:
    insert.alias(new_db, alias)
end = time.time()
elapsed = end - start
print('Aliases inserted in ' + str(elapsed) + ' seconds')

#
# LOGS
#
print('Disabling indexes on logs')
new_db.query(("UPDATE pg_index "
              "SET indisready=false "
              "WHERE indrelid = ( "
                "SELECT oid "
                "FROM pg_class "
                "WHERE relname='logs'"
              ");"
))

print('Disabling logging on logs')
new_db.query("ALTER TABLE logs SET UNLOGGED")

print('Disabling autovacuum')
new_db.query("ALTER TABLE logs SET (autovacuum_enabled = false, toast.autovacuum_enabled = false)")
print('Preparing logs...')
start = time.time()
logs = extract.logs(old_db)
removed_aliases = []
logs_ready = []
for log in logs:
    shorturl = log['alias']
    if shorturl in removed_aliases: continue
    alias = extract.alias_details(new_db, shorturl) or extract.code_details(new_db, shorturl)
    if alias is None:
        removed_aliases.append(shorturl)
        print(shorturl + ' has been removed, no logs for this one')
        continue
    log['alias_id'] = alias[0]
    log['url'] = alias[1]
    logs_ready.append(log)
end = time.time()
elapsed = end - start
print('Logs ready in ' + str(elapsed) + ' seconds')

print('Inserting logs...')
start = time.time()
req_count = 0
for log in logs_ready:
    insert.log(new_db, log)
    req_count += 1
    if req_count >= 1000000:
        print("Committing 1 million rows...")
        new_db.commit()
        req_count = 0
end = time.time()
elapsed = end - start
print('Logs inserted in ' + str(elapsed) + ' seconds')

print('Re-enabling autovacuum')
new_db.query("ALTER TABLE logs SET (autovacuum_enabled = true, toast.autovacuum_enabled = true)")

print('Re-enabling logging on logs')
new_db.query("ALTER TABLE logs SET LOGGED")

print('Re-enabling indexes on logs')
new_db.query(("UPDATE pg_index "
              "SET indisready=true "
              "WHERE indrelid = ( "
                "SELECT oid "
                "FROM pg_class "
                "WHERE relname='logs'"
              ");"
))
new_db.query("REINDEX TABLE logs;")
