from datetime import datetime

def gen_placeholder(length):
    return "("+', '.join(['%s']*length)+")"

def url(db, alias):
    query = 'INSERT INTO urls (url, created_at, updated_at) VALUES '
    values = [
                 alias['url'], 
                 str(alias['created_at']),
                 str(datetime.now())
             ]
    placeholder = gen_placeholder(len(values))
    return db.insert_commit(query + placeholder, values)

def alias(db, alias):
    query = 'INSERT INTO aliases (alias, code, created_at, updated_at, url_id, hidden) VALUES '
    values = [
                  (alias['alias'], alias['code'])[alias['only_code']],
                  (alias['code'] , None)         [alias['only_code']],
                  alias['created_at'], 
                  str(datetime.now()),
                  alias['url_id'],
                  bool(alias['hidden'])
             ]
    placeholder = gen_placeholder(len(values))
    db.insert_commit(query + placeholder, values)

def log(db, log):
    query = 'INSERT INTO logs '
    fields =  '(click_time, referrer, user_agent, ip_address, iso_code, alias_id, alias, url) '
    query = query + fields + 'VALUES '
    values = [
        log['click_time'],
        log['referrer'],
        log['user_agent'],
        log['ip_address'],
        log['iso_code'],
        log['alias_id'],
        log['alias'],
        log['url']
    ]
    placeholder = gen_placeholder(len(values))
    db.insert(query + placeholder, values)
