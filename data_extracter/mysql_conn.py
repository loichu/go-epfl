import mysql.connector
from mysql.connector import MySQLConnection, Error

class MySQLCursorDict(mysql.connector.cursor.MySQLCursor):
  def fetchone(self):
    row = self._fetch_row()
    if row:
      return dict(zip(self.column_names, self._row_to_python(row)))
    return None

class MySqlDB():
    def __init__(self, config):
        self.conn   = MySQLConnection(**config)
        self.cursor = self.conn.cursor(buffered=True, cursor_class=MySQLCursorDict)

    def query(self, query):
        self.cursor.execute(query)

    def close(self):
        self.cursor.close()
        self.conn.close()

    def commit(self):
        self.conn.commit()

    def fetchmany(self, nb_rows):
        return self.cursor.fetchmany(nb_rows)

    def fetchall(self):
        columns = self.cursor.description
        result = [{columns[index][0]:column for index, column in enumerate(value)} for value in    self.cursor.fetchall()]
        return result

    def fetchone(self):
        return self.cursor.fetchone()

if __name__ == '__main__':
    config = {
      'user': 'user',
      'password': 'password',
      'host': '127.0.0.1',
      'database': 'go',
      'raise_on_warnings': True
    }
    db = MySqlDB(config)
    db.query("SELECT * FROM phurl_urls;")
    result = db.fetchone()
    db.close()
    result != None
