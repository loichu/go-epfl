from postgres_conn import PostgresDB
from mysql_conn import MySqlDB
import time
import extract
import insert

config_old_db = {
  'user': '**user**',
  'password': '**password**',
  'port': '3306',
  'host': '127.0.0.1',
  'database': 'go',
  'raise_on_warnings': True
}
old_db = MySqlDB(config_old_db)

config_new_db = {
  'dbname': 'goepfl', 
  'user': '**user**', 
  'password': '**password**', 
  'host': 'localhost'
}
new_db = PostgresDB(config_new_db)

#
# ALIASES
#
print('Preparing aliases...')
start = time.time()
last_inserted_date = extract.last_alias_date(new_db)
aliases = extract.last_urls(old_db, str(last_inserted_date[0]))
for alias in aliases:
    alias['url_id'] = extract.url_id(new_db, alias['url'])
    if alias['url_id'] == None:
        alias['url_id'] = insert.url(new_db, alias)
end = time.time()
elapsed = end - start
print('Aliases ready in ' + str(elapsed) + ' seconds')

print('Inserting aliases...')
start = time.time()
for alias in aliases:
    insert.alias(new_db, alias)
end = time.time()
elapsed = end - start
print('Aliases inserted in ' + str(elapsed) + ' seconds')

#
# LOGS
#
print('Preparing logs...')
start = time.time()
last_inserted_date = extract.last_log_date(new_db)
logs = extract.last_logs(old_db, str(last_inserted_date[0]))
removed_aliases = []
logs_ready = []
for log in logs:
    shorturl = log['alias']
    if shorturl in removed_aliases: continue
    alias = extract.alias_details(new_db, shorturl) or extract.code_details(new_db, shorturl)
    if alias is None:
        removed_aliases.append(shorturl)
        print(shorturl + ' has been removed, no logs for this one')
        continue
    log['alias_id'] = alias[0]
    log['url'] = alias[1]
    logs_ready.append(log)
end = time.time()
elapsed = end - start
print('Logs ready in ' + str(elapsed) + ' seconds')

print('Inserting logs...')
start = time.time()
req_count = 0
for log in logs_ready:
    insert.log(new_db, log)
    req_count += 1
    if req_count >= 1000000:
        print("Committing 1 million rows...")
        new_db.commit()
        req_count = 0
new_db.commit()
end = time.time()
elapsed = end - start
print('Logs inserted in ' + str(elapsed) + ' seconds')
