import sys

def urls(db):
    db.query("SELECT * FROM phurl_urls")
    urls = db.fetchall()
    return prepare_aliases(urls)

def logs(db):
    db.query("SELECT * FROM phurl_log")
    logs = db.fetchall()
    return prepare_logs(logs)

def prepare_aliases(urls):
    aliases = []
    for url in urls:
        alias = {}
        alias['alias'] = url['alias'].decode("utf-8")
        alias['code'] = url['code'].decode("utf-8")
        alias['only_code'] = (False, True)[alias['alias'] == '']
        alias['created_at'] = str(url['date_added'])
        alias['url'] = url['url'].decode("utf-8")
        alias['hidden'] = url['private']
        aliases.append(alias)
    return aliases

def prepare_logs(logs):
    new_logs = []
    for log in logs:
        new_log = {}
        new_log['click_time'] = log['click_time']
        new_log['referrer'] = log['referrer']
        new_log['user_agent'] = log['user_agent']
        new_log['ip_address'] = log['ip_address']
        new_log['iso_code'] = log['country_code']
        new_log['alias'] = log['shorturl']
        new_logs.append(new_log)
    return new_logs

def url_id(db, url):
    query = "SELECT id FROM urls WHERE url=%s"
    db.query(query, [url])
    id = db.fetchone()
    if id == None: return None
    return ''.join(map(str, id))

def alias_details(db, alias):
    query = "SELECT a.id, u.url FROM aliases a LEFT JOIN urls u ON a.url_id = u.id WHERE alias=%s"
    db.query(query, [alias])
    return db.fetchone()

def code_details(db, code):
    query = "SELECT a.id, u.url FROM aliases a LEFT JOIN urls u ON a.url_id = u.id WHERE code=%s"
    db.query(query, [code])
    return db.fetchone()

def last_urls(db, last_date):
    db.query("SELECT * FROM phurl_urls WHERE date_added > \"" + last_date + "\"" )
    urls = db.fetchall()
    return prepare_aliases(urls)

def last_logs(db, last_date):
    query = "SELECT * FROM phurl_log WHERE click_time > "
    date = "\"" + last_date.rsplit('+')[0] + "\""
    db.query(query + date)
    logs = db.fetchall()
    return prepare_logs(logs)

def last_log_date(db):
    db.query("SELECT click_time FROM logs ORDER BY click_time DESC LIMIT 1")
    return db.fetchone()

def last_alias_date(db):
    db.query("SELECT created_at FROM urls ORDER BY created_at DESC LIMIT 1")
    return db.fetchone()
